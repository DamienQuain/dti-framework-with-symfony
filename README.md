# DTI Framework with Symfony

The architecture framework inspired by the SOLID principle, clean architecture
 and hexagonal architecture

The objective and facilitate development by pooling the technical part but with freedom of implementation
and isolate the domain from the rest of the code to be able to code in DDD and easily test the business

is easy to protect the business code and share the technical stack on in the  open source

is simple to dev cloud native application and use this for microservice.
or start with monolith and extract functionality to microservice in the future.
and it is possible to refacto part of the code without big breaking change in many cases.

# SOLID :
## S - Single Responsibility Principle (known as SRP)
    code is Isolate to the feature and the framework avoids code duplication as much as possible 

## O - Open/Closed Principle.
    use the trait for mutualise functionality, the code is generics and use the interface for require methods 

## L - Liskov's Substitution Principle.
    use PHP 8.0 with type for secure the code
    is easy to extands class

## I - Interface Segregation Principle.
    use interface for require functionnality. and not use big interface

## D - Dependency Inversion Principle.
    use maximum the interface if is disponible

# Clean Architecture and Hexagonal Architecture

the must importante rules in this framework is the isolation of the Domain the domain must not depands of the infra or technical class 
( exception for the attributes or the specifiques features example :  one function for generate one uuid )

l'infra depands du technique et l'implémentation du technique du domaine 



# start this project
## Start with docker 

docker-compose up -d
( for this version is only use docker for dev)

## project Architecture

```
src
    - Domain
        Geolocation
            - object Addresse, City
            - serviceGeolocation (domain function)
            - GeolocationPort( method used in domain service and exploit technical class)
        Media
            - MediaInterface
    - Infra
        - database
            - Entity
            - Repository
            - Migration
        - File
        - Search
            - Adapter (implement Technical Ports)
            - Repository
        -....Other external utility
    - Technical
        - Controller ( is endpoind for project)
            - Api
                - v0
                 - Auth
                 - Geolocation
                - v1
        - Security
            - Command
            - Service (for complexe method)
            - Adapter (implement domain Ports)
            - Ports 
        - Serializer
        - Common
        
```


# package util 

composer require nette/php-generator ( use this for generate php class entity)

composer require hotmeteor/spectator --dev https://github.com/hotmeteor/spectator (for test open api spec )