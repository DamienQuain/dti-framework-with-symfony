<?php

namespace Domain\Media;

use Technical\Serializer\TraitsSerializerInterface;
use Technical\DataManager\Attributes as DM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation as Serialize;

trait MediasTrait
{
    #[DM\Persist(containType: MediaInterface::class)]
    #[DM\Link(type: 'ManyToMany')]
    private iterable $medias;

    public function __construct()
    {
        $this->medias = [];
    }

    /**
     * @return MediaInterface[]
     */
    #[Serialize\Groups([TraitsSerializerInterface::READ,TraitsSerializerInterface::UPDATE])]
    public function getMedias(): iterable
    {
        return $this->medias;
    }

    /**
     * add media
     * @param MediaInterface $media
     * @return self
     */
    public function addMedia(MediaInterface $media): self
    {
        $medias =  iterator_to_array($this->medias);
        if (!in_array($media, $medias, true)) {
            $this->medias[] = $media;
        }

        return $this;
    }

    /**
     * remove media
     * @param MediaInterface $media
     * @return self
     */
    public function removeMedia(MediaInterface $media): self
    {
        $medias =  iterator_to_array($this->medias);
        if ($key = array_search($media, $medias, true)) {
            unset($this->medias[$key]);
        }

        return $this;
    }

}
