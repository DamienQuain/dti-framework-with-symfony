<?php

namespace Domain\Media;

use Technical\Serializer\TraitsSerializerInterface;
use Technical\DataManager\Attributes as DM;
use Symfony\Component\Serializer\Annotation as Serialize;

trait MediaTrait
{
    #[DM\Link(type: 'OneToOne')]
    private ?MediaInterface $media = null;

    /**
     * @return MediaInterface
     */
    #[Serialize\Groups([TraitsSerializerInterface::MINIMAL])]
    public function getMedia(): ?MediaInterface
    {
        return $this->media;
    }

    /**
     * @param MediaInterface|null $media
     * @return self
     */
    #[Serialize\Groups([TraitsSerializerInterface::UPDATE])]
    public function setMedia(?MediaInterface $media): self
    {
        $this->media = $media;
        return $this;
    }


    

}
