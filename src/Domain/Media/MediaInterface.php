<?php

namespace Domain\Media;

interface MediaInterface
{
    const MINIMAL = 'media:minimal';
    const CREATE_ONLY = 'media:create';
    const READ = 'media:read';
    const UPDATE = 'media:update';

    // Serialize group
    const GROUP_MINIMAL = [
        self::MINIMAL
    ];

    const GROUP_READ = [
        ...self::GROUP_MINIMAL,
        self::READ
    ];

    const GROUP_UPDATE = [
        self::UPDATE
    ];

    const GROUP_CREATE = [
        ...self::GROUP_UPDATE,
        self::CREATE_ONLY
    ];
}
