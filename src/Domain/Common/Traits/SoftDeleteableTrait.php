<?php

namespace Domain\Common\Traits;

use Technical\DataManager\Attributes as DM;
use Domain\Common\Ports\DataManagerPort;
use DateTime;

trait SoftDeleteableTrait
{
    private ?DateTime $deletedAt;

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    #[DM\Event(name:'preSync')]
	public function beforeSaveOnDataManager(object $event)
	{
        //DataManagerPort::applyEvent('preSync',$event);
	}
}
