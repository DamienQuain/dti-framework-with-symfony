<?php

namespace Domain\Common\Traits;

use Technical\Serializer\TraitsSerializerInterface;
// is exception use this for generate uuid
use Technical\DataManager\Attributes as DM;
// is exception use this for generate uuid
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Serializer\Annotation as Serialize;

trait IdTrait
{
    protected ?int $id;

    #[DM\Persist(unique: true)]
    protected string $uuid;

    public function __construct() 
    {
        $this->uuid = Uuid::v4()->toRfc4122();
    }

    /**
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @return string|null
     */
    #[Serialize\Groups([TraitsSerializerInterface::MINIMAL])]
    public function getUuid(): ?string
    {
        return $this->uuid;
    }
}
