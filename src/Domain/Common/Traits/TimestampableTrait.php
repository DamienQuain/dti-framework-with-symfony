<?php

namespace Domain\Common\Traits;

use Technical\Serializer\TraitsSerializerInterface;
use Technical\DataManager\Attributes as DM;
use Symfony\Component\Serializer\Annotation as Serialize;
use DateTime;

trait TimestampableTrait
{
    #[DM\Persist(options: ["default" => "CURRENT_TIMESTAMP"])]
    private ?DateTime $createdAt = null;

    #[DM\Persist(options: ["default" => "CURRENT_TIMESTAMP"])]
    private ?DateTime $updatedAt = null;

    /**
     * @return void
     */
    #[Serialize\Groups([TraitsSerializerInterface::READ])]
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return void
     */
    #[Serialize\Groups([TraitsSerializerInterface::READ])]
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }



    #[DM\Event(name:'preCreate')]
	public function beforeFirstSave()
	{
		$this->createdAt = new \DateTime("now");
	}

	#[DM\Event(name:'preUpdate')]
	public function beforeUpdate()
	{
		$this->updatedAt = new \DateTime("now");
	}
}
