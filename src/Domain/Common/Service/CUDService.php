<?php

namespace Domain\Common\Service;

use Domain\Common\Ports\ValidatorPort;
use Domain\Common\Ports\DataManagerPort;
use Domain\Common\Ports\CUDPort;

class CUDService implements CUDPort
{
    public function __construct(
        protected DataManagerPort $dm,
        protected ValidatorPort $validator
    )
    {
        
    }
    public function create(object &$object, array $context = [])
    {
        $this->validator->validate($object);
        $this->dm->save($object);
    }

    public function update(object $object, array $context = [])
    {
        $this->validator->validate($object);
        $this->dm->save($object);
    }

    public function delete(object $object, array $context = [])
    {
        $this->dm->remove($object);
    }
}
