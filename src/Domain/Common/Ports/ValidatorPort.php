<?php

namespace Domain\Common\Ports;

interface ValidatorPort
{
    public function validate(object $object,array|null $constraints=null, array|null $groups=null);
}
