<?php

namespace Domain\Common\Ports;

interface DataManagerPort
{
    public function save(object $object):bool;
    public function remove(object $object):bool;
    public function load(string $classname,string $uuid):?object;
    public function getRepository(string $classname);

    public static function applyEvent(string $eventName,object $event);
}
