<?php

namespace Domain\Common\Ports;

interface CUDPort
{
    public function create(object &$object, array $context = []);

    public function update(object $object, array $context = []);

    public function delete(object $object, array $context = []);
}
