<?php

namespace Domain\User;

use Technical\Serializer\TraitsSerializerInterface;
use Technical\DataManager\Attributes as DM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation as Serialize;
use Domain\Media\MediaTrait;
use Domain\Media\MediaInterface;
use Domain\Common\Traits\TimestampableTrait;
use Domain\Common\Traits\IdTrait;

#[DM\Persist]
class User
{
    const MINIMAL = 'user:minimal';
    const CREATE_ONLY = 'user:create';
    const READ = 'user:read';
    const UPDATE = 'user:update';
    const AUTH = 'user:auth';

    // Serialize group
    const GROUP_MINIMAL = [
        ...MediaInterface::GROUP_MINIMAL,
        TraitsSerializerInterface::MINIMAL,
        self::MINIMAL
    ];

    const GROUP_READ = [
        ...self::GROUP_MINIMAL,
        ...CGU::GROUP_MINIMAL,
        self::READ
    ];

    const GROUP_UPDATE = [
        self::UPDATE
    ];

    const GROUP_CREATE = [
        ...self::GROUP_UPDATE,
        self::CREATE_ONLY
    ];

    use IdTrait {
        IdTrait::__construct as private __idConstruct;
    }
    use TimestampableTrait;
    use MediaTrait;

    #[Serialize\SerializedName("avatar")]
    #[DM\Link(type: 'OneToOne')]
    private ?MediaInterface $media = null;
    
    private string $firstName;

    private string $lastName;

    #[DM\Persist(unique: true)]
    private string $phone;

    #[Assert\NotBlank(allowNull:true)]
    #[DM\Link(type: 'ManyToOne')]
    private ?CGU $acceptedCGU;

    #[DM\Persist(cascade:['persist','remove'])]
    #[DM\Link(type: 'OneToOne', mappedBy:'user')]
    private TechnicalUserInterface $technicalUser;

    #[DM\Persist(containType: Role::class)]
    #[DM\Link(type: 'ManyToMany')]
    private iterable $roles;
    
    public function __construct() 
    {
      $this->__idConstruct();
    }
    
    /**
     *
     * @return string|null
     */
    #[Serialize\Groups([self::MINIMAL])]
    public function getEmail(): ?string
    {
        return $this->technicalUser->getEmail();
    }

    /**
     * Set Email
     * @param string $email
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setEmail(string $email): self
    {
        if(isset($this->technicalUser)) {
            $this->technicalUser->setEmail($email);
        }
        return $this;
    }

    #[Serialize\Groups([self::READ])]
    public function getRoles(): iterable
    {
        return $this->roles;
    }

    /**
     * Add new role
     *
     * @param Role $role
     * @return void
     */
    public function addRole(Role $role): self
    {
        $this->roles[]=$role;

        return $this;
    }

    public function removeRole(Role $role): self
    {
        $roles = iterator_to_array($this->roles);
        if (($key = array_search($role, $roles)) !== false) {
            unset($this->roles[$key]);
        }
        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string|null
     */
    #[Serialize\Groups([self::MINIMAL])]
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    #[Serialize\Groups([self::MINIMAL])]
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    #[Serialize\Groups([self::READ])]
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return CGU|null
     */
    #[Serialize\Groups([self::READ])]
    public function getAcceptedCGU(): ?CGU
    {
        return $this->acceptedCGU;
    }

    /**
     * @param CGU $acceptedCGU
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setAcceptedCGU(CGU $acceptedCGU): self
    {
        $this->acceptedCGU = $acceptedCGU;

        return $this;
    }

    public function getTechnicalUser(): TechnicalUserInterface
    {
        return $this->technicalUser;
    }

    public function setTechnicalUser(TechnicalUserInterface $technicalUser): self
    {
        $this->technicalUser = $technicalUser;
        if ($technicalUser->getUser() == null) {
            $technicalUser->setUser($this);
        }
        return $this;
    }

}
