<?php

namespace Domain\User;

use Technical\DataManager\Attributes as DM;
use Domain\Common\Traits\IdTrait;

#[DM\Persist]
class Role
{

    const MINIMAL = 'role:minimal';
    const CREATE_ONLY = 'role:create';
    const READ = 'role:read';
    const UPDATE = 'role:update';

    // Serialize group
    const GROUP_MINIMAL = [
        self::MINIMAL
    ];

    const GROUP_READ = [
        ...self::GROUP_MINIMAL,
        self::READ
    ];

    const GROUP_UPDATE = [
        self::UPDATE
    ];

    const GROUP_CREATE = [
        ...self::GROUP_UPDATE,
        self::CREATE_ONLY
    ];
    
    use IdTrait;
    
    /**
     * this fields is the role in the technical
     * @var TechnicalRoleInterface
     */
    #[DM\Link(type: 'OneToOne', mappedBy:'role')]
    private TechnicalRoleInterface $technicalRole;

    /**
     * @return TechnicalRoleInterface
     */
    public function getTechnicalRole(): TechnicalRoleInterface
    {
        return $this->technicalRole;
    }

    /**
     * @return self
     */
    public function setTechnicalRole(TechnicalRoleInterface $technicalRole): self
    {
        $this->technicalRole = $technicalRole;
        if ($technicalRole->getRole() !== $this) {
            $technicalRole->setRole($this);
        }
        return $this;
    }
}
