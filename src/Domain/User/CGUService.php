<?php
namespace Domain\User;

use Domain\Common\Service\CUDService;

class CGUService extends CUDService
{
    public function update(object $cgu, array $context = [])
    {
        if(!$cgu instanceof CGU) {
            // return the exception of type
        }
        $cgu = clone $cgu;
        $cgu->setVersion($cgu->getVersion()+1);
        parent::update($cgu);
    }
}