<?php
namespace Domain\User;

use Technical\DataManager\Attributes as DM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Serializer\Annotation as Serialize;
use Domain\Common\Traits\TimestampableTrait;
use Domain\Common\Traits\SoftDeleteableTrait;
use Domain\Common\Traits\IdTrait;

#[DM\Persist]
class CGU 
{
    const MINIMAL = 'cgu:minimal';
    const CREATE_ONLY = 'cgu:create';
    const READ = 'cgu:read';
    const UPDATE = 'cgu:update';

    // Serialize group
    const GROUP_MINIMAL = [
        self::MINIMAL
    ];

    const GROUP_READ = [
        ...self::GROUP_MINIMAL,
        self::READ
    ];

    const GROUP_UPDATE = [
        self::UPDATE
    ];

    const GROUP_CREATE = [
        ...self::GROUP_UPDATE,
        self::CREATE_ONLY
    ];

    use IdTrait;
    use TimestampableTrait;
    use SoftDeleteableTrait;

    #[DM\Persist(length: 1000)]
    private string $cGU;

    private string $type;
    
    private int $version = 0;


     /**
     * get Description
     * @return string|null
     */
    #[Serialize\Groups([self::READ])]
    public function getCGU(): ?string
    {
        return $this->cGU;
    }

    /**
     * get cGU
     * @param string|null $description
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setCGU(?string $cGU): self
    {
        $this->cGU = $cGU;

        return $this;
    }

    /**
     * get type
     * @return string|null
     */
    #[Serialize\Groups([self::MINIMAL])]
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * set type
     *
     * @param string $type
     * @return self
     */
    #[Serialize\Groups([self::CREATE_ONLY])]
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * get version
     * @return int|null
     */
    #[Serialize\Groups([self::MINIMAL])]
    public function getVersion(): ?int
    {
        return $this->version;
    }

    /**
     * set type
     * @param int $type
     * @return self
     */
    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function __clone()
    {
        $this->id = null;
        $this->uuid = Uuid::v4()->toRfc4122();
    }

}
