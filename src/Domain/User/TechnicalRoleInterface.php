<?php

namespace Domain\User;

interface TechnicalRoleInterface
{
    public const USER = 'ROLE_USER';
    public const ADMIN = 'ROLE_ADMIN'; 
    
    const MINIMAL = 'technicalRole:minimal';
    const CREATE_ONLY = 'technicalRole:create';
    const READ = 'technicalRole:read';
    const UPDATE = 'technicalRole:update';

    // Serialize group
    const GROUP_MINIMAL = [
        self::MINIMAL
    ];

    const GROUP_READ = [
        ...self::GROUP_MINIMAL,
        self::READ
    ];

    const GROUP_UPDATE = [
        self::UPDATE
    ];

    const GROUP_CREATE = [
        ...self::GROUP_UPDATE,
        self::CREATE_ONLY
    ];

    public function getRole(): ?Role;
    public function setRole(?Role $Role): self;
}
