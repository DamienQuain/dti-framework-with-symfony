<?php

namespace Domain\User;

interface TechnicalUserInterface
{
    const MINIMAL = 'technicalUser:minimal';
    const CREATE_ONLY = 'technicalUser:create';
    const READ = 'technicalUser:read';
    const UPDATE = 'technicalUser:update';

    // Serialize group
    const GROUP_MINIMAL = [
        self::MINIMAL
    ];

    const GROUP_READ = [
        ...self::GROUP_MINIMAL,
        self::READ
    ];

    const GROUP_UPDATE = [
        self::UPDATE
    ];

    const GROUP_CREATE = [
        ...self::GROUP_UPDATE,
        self::CREATE_ONLY
    ];

    public function getUser(): ?User;
    public function setUser(?User $user): self;

    public function getEmail(): ?string;
    public function setEmail(string $email): self;

}