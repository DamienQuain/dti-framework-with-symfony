<?php

namespace Infra\Database\Exception;

use InvalidArgumentException;

/**
 * Class MissingClassParameterException
 *
 */
class MissingClassParameterException extends InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct(
            'Missing "__class" key value for "json_object" type field'
        );
    }
}