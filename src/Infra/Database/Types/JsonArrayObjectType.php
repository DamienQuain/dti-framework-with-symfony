<?php

namespace Infra\Database\Types;

use Technical\Common\Service\ToolsService;
use ReflectionClass;
use Reflection;
use Infra\Database\Exception\MissingClassParameterException;
use Doctrine\DBAL\Types\JsonType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class JsonObjectType
 */
class JsonArrayObjectType extends JsonType
{
    const JSON_OBJECT = 'json_array_object';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return parent::getSQLDeclaration($fieldDeclaration, $platform);
    }

    /**
     * @inheritDoc
     */
    public function convertToDatabaseValue($values, AbstractPlatform $platform)
    {
        $data=[];
        if(!empty($values)) {
            foreach($values as $value) {
                $reflexionClass = new ReflectionClass($value::class);
                $reflexionProperties = ToolsService::getAllReflexionProperties($reflexionClass);
                $item=[];
                foreach ($reflexionProperties as $reflexionProperty) {
                    $reflexionProperty->setAccessible(true);
                    $item[$reflexionProperty->getName()] = $reflexionProperty->getValue($value);
                }
                $item['__class'] = get_class($value);
                $data[]=$item;
            }
        }
        return parent::convertToDatabaseValue($data, $platform);
    }

    /**
     * @inheritDoc
     *
     * @throws MissingClassParameterException
     */
    public function convertToPHPValue($values, AbstractPlatform $platform)
    {
        $data = parent::convertToPHPValue($values, $platform);

        if (null === $data) {
            return null;
        }

        foreach($data as $key => $item) {
            if(isset($item['__class'])) {
                $reflexionClass = new ReflectionClass($item['__class']);
                $reflexionProperties = ToolsService::getAllReflexionProperties($reflexionClass);
                $object=new $item['__class']();
                foreach ($reflexionProperties as $reflexionProperty) {
                    $reflexionProperty->setAccessible(true);
                    $reflexionProperty->setValue($object,$item[$reflexionProperty->getName()]);
                }
                $data[$key] = $object;
            }
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::JSON_OBJECT;
    }

    /**
     * @inheritDoc
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
