<?php

namespace Infra\Database\Types;

use Technical\Common\Service\ToolsService;
use ReflectionClass;
use Infra\Database\Exception\{
    MissingClassParameterException
};
use Doctrine\DBAL\Types\JsonType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class JsonObjectType
 */
class JsonObjectType extends JsonType
{
    const JSON_OBJECT = 'json_object';

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return parent::getSQLDeclaration($fieldDeclaration, $platform);
    }

    /**
     * @inheritDoc
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $reflexionClass = new ReflectionClass($value::class);
        $reflexionProperties = ToolsService::getAllReflexionProperties($reflexionClass);
        $data=[];
        foreach ($reflexionProperties as $reflexionProperty) {
            $reflexionProperty->setAccessible(true);
            $data[$reflexionProperty->getName()] = $reflexionProperty->getValue($value);
        }
        $data['__class'] = get_class($value);

        return parent::convertToDatabaseValue($data, $platform);
    }

    /**
     * @inheritDoc
     *
     * @throws MissingClassParameterException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $data = parent::convertToPHPValue($value, $platform);

        if (null === $data) {
            return null;
        }

        if (false === isset($data['__class'])) {
            throw new MissingClassParameterException();
        }

        $reflexionClass = new ReflectionClass($data['__class']);
        $reflexionProperties = ToolsService::getAllReflexionProperties($reflexionClass);
        $object=new $data['__class']();
        foreach ($reflexionProperties as $reflexionProperty) {
            $reflexionProperty->setAccessible(true);
            $reflexionProperty->setValue($object,$data[$reflexionProperty->getName()]);
        }

        return $object;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::JSON_OBJECT;
    }

    /**
     * @inheritDoc
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
