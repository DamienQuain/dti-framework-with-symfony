<?php
namespace Infra\Database\Filters;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\ORM\Mapping\ClassMetaData;
class SoftDeleteableFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->hasField("deletedAt")) {
            $date = date("Y-m-d H:i:s");
            return $targetTableAlias.".deleted_at < '".$date."' OR ".$targetTableAlias.".deleted_at IS NULL";
        }
        return "";
    }
}