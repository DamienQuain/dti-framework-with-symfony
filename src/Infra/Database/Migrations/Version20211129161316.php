<?php

declare(strict_types=1);

namespace Infra\Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211129161316 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE t_technical_role ADD role_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE t_technical_role ADD CONSTRAINT FK_972A2E8AD60322AC FOREIGN KEY (role_id) REFERENCES d_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_972A2E8AD60322AC ON t_technical_role (role_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_technical_role DROP CONSTRAINT FK_972A2E8AD60322AC');
        $this->addSql('DROP INDEX UNIQ_972A2E8AD60322AC');
        $this->addSql('ALTER TABLE t_technical_role DROP role_id');
    }
}
