<?php

declare(strict_types=1);

namespace Infra\Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211129130346 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE d_role_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE d_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE dcgu_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE t_media_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE t_technical_role_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE t_technical_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE d_role (id INT NOT NULL, uuid VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EEC9E183D17F50A6 ON d_role (uuid)');
        $this->addSql('CREATE TABLE d_user (id INT NOT NULL, media_id INT DEFAULT NULL, accepted_cgu_id INT DEFAULT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, uuid VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3433BDA0444F97DD ON d_user (phone)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3433BDA0D17F50A6 ON d_user (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3433BDA0EA9FDD75 ON d_user (media_id)');
        $this->addSql('CREATE INDEX IDX_3433BDA029B98456 ON d_user (accepted_cgu_id)');
        $this->addSql('CREATE TABLE user_role (user_id INT NOT NULL, role_id INT NOT NULL, PRIMARY KEY(user_id, role_id))');
        $this->addSql('CREATE INDEX IDX_2DE8C6A3A76ED395 ON user_role (user_id)');
        $this->addSql('CREATE INDEX IDX_2DE8C6A3D60322AC ON user_role (role_id)');
        $this->addSql('CREATE TABLE dcgu (id INT NOT NULL, gu TEXT NOT NULL, type VARCHAR(255) NOT NULL, version INT NOT NULL, uuid VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D542B2E2D17F50A6 ON dcgu (uuid)');
        $this->addSql('CREATE TABLE t_media (id INT NOT NULL, directory VARCHAR(255) DEFAULT NULL, file_path VARCHAR(255) NOT NULL, file_size INT NOT NULL, file_extension VARCHAR(255) NOT NULL, file_mime_type VARCHAR(255) NOT NULL, uuid VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D49DC775D17F50A6 ON t_media (uuid)');
        $this->addSql('CREATE TABLE t_refresh_token (token VARCHAR(255) NOT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(token))');
        $this->addSql('CREATE TABLE t_technical_role (id INT NOT NULL, name VARCHAR(255) NOT NULL, uuid VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_972A2E8A5E237E06 ON t_technical_role (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_972A2E8AD17F50A6 ON t_technical_role (uuid)');
        $this->addSql('CREATE TABLE t_technical_user (id INT NOT NULL, user_id INT DEFAULT NULL, roles JSON NOT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, uuid VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4DD072A9E7927C74 ON t_technical_user (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4DD072A9D17F50A6 ON t_technical_user (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4DD072A9A76ED395 ON t_technical_user (user_id)');
        $this->addSql('ALTER TABLE d_user ADD CONSTRAINT FK_3433BDA0EA9FDD75 FOREIGN KEY (media_id) REFERENCES t_media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE d_user ADD CONSTRAINT FK_3433BDA029B98456 FOREIGN KEY (accepted_cgu_id) REFERENCES dcgu (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES d_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3D60322AC FOREIGN KEY (role_id) REFERENCES d_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE t_technical_user ADD CONSTRAINT FK_4DD072A9A76ED395 FOREIGN KEY (user_id) REFERENCES d_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_role DROP CONSTRAINT FK_2DE8C6A3D60322AC');
        $this->addSql('ALTER TABLE user_role DROP CONSTRAINT FK_2DE8C6A3A76ED395');
        $this->addSql('ALTER TABLE t_technical_user DROP CONSTRAINT FK_4DD072A9A76ED395');
        $this->addSql('ALTER TABLE d_user DROP CONSTRAINT FK_3433BDA029B98456');
        $this->addSql('ALTER TABLE d_user DROP CONSTRAINT FK_3433BDA0EA9FDD75');
        $this->addSql('DROP SEQUENCE d_role_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE d_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE dcgu_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE t_media_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE t_technical_role_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE t_technical_user_id_seq CASCADE');
        $this->addSql('DROP TABLE d_role');
        $this->addSql('DROP TABLE d_user');
        $this->addSql('DROP TABLE user_role');
        $this->addSql('DROP TABLE dcgu');
        $this->addSql('DROP TABLE t_media');
        $this->addSql('DROP TABLE t_refresh_token');
        $this->addSql('DROP TABLE t_technical_role');
        $this->addSql('DROP TABLE t_technical_user');
    }
}
