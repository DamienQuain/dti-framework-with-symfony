<?php

namespace Infra\Database\Repository\Domain\User;

use Domain\User\CGU;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method CGU|null find($id, $lockMode = null, $lockVersion = null)
 * @method CGU|null findOneBy(array $criteria, array $orderBy = null)
 * @method CGU[]    findAll()
 * @method CGU[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CGURepository extends ServiceEntityRepository
{   
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CGU::class);
    }

    public function findLast(string $type)
    {
        return $this->findOneBy(['type'=>$type],['version' => 'DESC']);
    }
}