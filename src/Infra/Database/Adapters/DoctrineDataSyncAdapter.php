<?php

namespace Infra\Database\Adapters;

use Technical\File\Generator\FileXmlGeneratorService;
use Technical\DataManager\ExternalDataSyncPort;
use Technical\Common\Service\ToolsService;
use Exception;
use DateTime;

class DoctrineDataSyncAdapter implements ExternalDataSyncPort
{
    private const BASE_ENTITY_PATH = '/var/www/src/Infra/Database/Entity';
    public const BASE_REPOSITORY_NAMESPACE = 'Infra\Database\Repository\\';

    private static $specifiqueType = [
        DateTime::class
    ];

    public static function syncObjecsMetadata(array $metadatasEntities)
    {
        $xmlsMetadata=[];
        foreach($metadatasEntities as $className => $metadataEntity){
            if($metadataEntity['type'] != 'entity' || $metadataEntity['format'] != 'data') {
                continue;
            }
            $content = [
                'doctrine-mapping' => [
                    'xmlns' => 'https://doctrine-project.org/schemas/orm/doctrine-mapping',
                    'xmlns:xsi' => 'https://www.w3.org/2001/XMLSchema-instance',
                    'xsi:schemaLocation' => 'https://doctrine-project.org/schemas/orm/doctrine-mapping https://www.doctrine-project.org/schemas/orm/doctrine-mapping.xsd'
                ]
            ];
            $entity = [
                'name'=>$metadataEntity['namespace'].'\\'.$metadataEntity['className'],
                'table'=>ToolsService::camelCaseToSnakeCase($metadataEntity['classGroup'][0].$metadataEntity['className'])
            ];

            $repositoryClass=self::BASE_REPOSITORY_NAMESPACE.$metadataEntity['namespace'].'\\'.$metadataEntity['className'].'Repository';
            if(class_exists($repositoryClass)){
                $entity['repository-class'] = $repositoryClass;
            }
            if(!is_null($metadataEntity['parent']) && isset($metadatasEntities[$metadataEntity['parent']])) {
                $metadataParentkey = $metadataEntity['parent'];
                while(!is_null($metadatasEntities[$metadataParentkey]['parent']) && isset($metadatasEntities[$metadataParentkey]['parent'])) {
                    $metadataParentkey = $metadatasEntities[$metadataParentkey]['parent'];
                }
                if(!isset($xmlsMetadata[$metadataParentkey])){
                    $xmlsMetadata[$metadataParentkey]=[
                        'content' => [
                            'doctrine-mapping' => [
                                'entity' => []
                            ]
                        ]
                    ];
                }
                self::addExtends($xmlsMetadata[$metadataParentkey]['content']['doctrine-mapping']['entity'],$metadataEntity);
            }
            self::addProperty($entity,$metadatasEntities,$metadataEntity);
            self::addLifecycleCallbacks($entity,$metadataEntity);
            $content['doctrine-mapping']['entity']=$entity;
            $fileName = self::BASE_ENTITY_PATH.'/'.$metadataEntity['classGroup'].'/';
            foreach(explode('\\',$metadataEntity['namespace']) as $key =>$space) {
                if($key !== 0){
                    $fileName.=$space.'.'; 
                }
            }
            $fileName.=$metadataEntity['className'].'.orm.xml';
            if(isset($xmlsMetadata[$className]['content'])){
                $content = array_merge_recursive($xmlsMetadata[$className]['content'],$content);
            }
            $xmlsMetadata[$className] = [
                'content'=> $content,
                'fileName' => $fileName
            ];
        }
        FileXmlGeneratorService::generatesFiles($xmlsMetadata);
    }

    private static function addLifecycleCallbacks(array &$xmlElements,array $metadataEntity) {
        $lifecycleCallbacks = [];
        foreach($metadataEntity['methods'] as $methodName => $metadataMethod){
            if(isset($metadataMethod['event'])){
                $type = match($metadataMethod['event']['name']){
                    'preCreate' => 'prePersist',
                    'preUpdate' => 'preUpdate',
                    'preSync' => 'preFlush'
                };
                $lifecycleCallbacks['lifecycle-callback_'.$methodName] = [
                    'type' => $type,
                    'method' => $methodName
                ];
            }
        }
        if(!empty($lifecycleCallbacks)){
            $xmlElements['lifecycle-callbacks']=$lifecycleCallbacks;
        }
    }
    private static function addProperty(array &$xmlElements,array $metadatasEntities ,array $metadataEntity) {

        
        $parentProperties=[];

        if(!is_null($metadataEntity['parent']) && isset($metadatasEntities[$metadataEntity['parent']])) {
            $metadataParentkey = $metadataEntity['parent'];
            while(!is_null($metadatasEntities[$metadataParentkey]['parent']) && isset($metadatasEntities[$metadataParentkey]['parent'])) {
                $parentProperties = array_merge($parentProperties,$metadatasEntities[$metadataParentkey]['properties']);
                $metadataParentkey = $metadatasEntities[$metadataParentkey]['parent'];
            }
            $parentProperties = array_merge($parentProperties,$metadatasEntities[$metadataParentkey]['properties']);
        }
        
        foreach($metadataEntity['properties'] as $propertyName => $metadataProperty)
        {
            if(array_key_exists($propertyName,$parentProperties)){
                continue;
            }
            if(self::addSpecifiquePropertyName($xmlElements,$metadataProperty,$propertyName)){
                continue;
            }
            if(class_exists($metadataProperty['type']) && !in_array($metadataProperty['type'],self::$specifiqueType)) {
                $format=$metadatasEntities[$metadataProperty['type']]['format'];
                if($format == 'data') {
                    self::addLink($xmlElements,$metadataProperty,$propertyName);
                }elseif($format == 'enum'){
                    if(!isset($xmlElements['field_'.$propertyName])){
                        $xmlElements['field_'.$propertyName] = [];
                    }
                    $xmlElements['field_'.$propertyName] = [
                        ...$xmlElements['field_'.$propertyName],
                        'name'=>$propertyName,
                        'type'=>'enum',
                        'column'=>ToolsService::camelCaseToSnakeCase($propertyName)
                    ];
                }elseif($format == 'json'){
                    if(!isset($xmlElements['field_'.$propertyName])){
                        $xmlElements['field_'.$propertyName] = [];
                    }
                    $xmlElements['field_'.$propertyName] = [
                        ...$xmlElements['field_'.$propertyName],
                        'name'=>$propertyName,
                        'type'=>'json_object',
                        'column'=>ToolsService::camelCaseToSnakeCase($propertyName)
                    ];
                }else {
                    dd('format is not valid: '+$format);
                }
                
            } elseif(isset($metadatasEntities[$metadataProperty['type']]) && $metadatasEntities[$metadataProperty['type']]['type'] == 'interface') {
                self::replaceInterface($metadatasEntities,$metadataEntity,$metadataProperty);
                self::addLink($xmlElements,$metadataProperty,$propertyName);
            } elseif(
                ($metadataProperty['type'] == 'array' || $metadataProperty['type'] == 'iterable')  && 
                isset($metadataProperty['constraint']['containType'])&&
                isset($metadatasEntities[$metadataProperty['constraint']['containType']])
            ){
                if($metadatasEntities[$metadataProperty['constraint']['containType']]['format'] == 'data') {
                    if($metadatasEntities[$metadataProperty['constraint']['containType']]['type'] == 'interface') {
                        self::replaceArrayInterface($metadatasEntities,$metadataEntity,$metadataProperty);
                    }
                    self::addLink($xmlElements,$metadataProperty,$propertyName);
                }elseif($metadatasEntities[$metadataProperty['constraint']['containType']]['format'] == 'json'){
                    if(!isset($xmlElements['field_'.$propertyName])){
                        $xmlElements['field_'.$propertyName] = [];
                    }
                    $xmlElements['field_'.$propertyName] = [
                        ...$xmlElements['field_'.$propertyName],
                        'name'=>$propertyName,
                        'column'=>ToolsService::camelCaseToSnakeCase($propertyName),
                        'type'=>'json_array_object'
                    ];
                }else {
                    dd('format is not valid: '+$metadatasEntities[$metadataProperty['constraint']['containType']]['format']);
                }
            } else {
                self::addclassicField($xmlElements,$metadataProperty,$propertyName);
            }
        }
    }

    private static function addLink(array &$xmlElement,array $metadataProperty, string $propertyName) 
    {  
        if(!isset($metadataProperty['constraint']['link']) || !isset($metadataProperty['constraint']['link']['type'])) {
            throw new Exception("the link type is required for the property $propertyName");
        }
       
        $relationType = match($metadataProperty['constraint']['link']['type']){
            'ManyToMany' => 'many-to-many',
            'OneToOne'=> 'one-to-one',
            'ManyToOne'=> 'many-to-one',
            'OneToMany'=> 'one-to-many'
        };

        if($metadataProperty['type'] == 'array'| $metadataProperty['type'] == 'iterable') {
            $targetEntity=$metadataProperty['constraint']['containType'];
        } else {
            $targetEntity=$metadataProperty['type'];
        }
        $xmlElement[$relationType.'_'.$propertyName] = [
            'field'=>$propertyName,
            'target-entity' => $targetEntity
        ]; 
        if(isset($metadataProperty['constraint']['link']['mappedBy'])) {
            $xmlElement[$relationType.'_'.$propertyName]['mapped-by'] = $metadataProperty['constraint']['link']['mappedBy'];
        }
        if(isset($metadataProperty['constraint']['link']['inversedBy'])) {
            $xmlElement[$relationType.'_'.$propertyName]['inversed-by'] = $metadataProperty['constraint']['link']['inversedBy'];
        }
        if(isset($metadataProperty['constraint']['cascade'])) {
            $cascade = [];
            foreach($metadataProperty['constraint']['cascade'] as $cascadeType ){
                $cascade['cascade-'.$cascadeType] = [];
            }
            $xmlElement[$relationType.'_'.$propertyName]['cascade'] = $cascade;
        }
        if(isset($metadataProperty['constraint']['orphanRemoval'])) {
            if($metadataProperty['constraint']['orphanRemoval']){
                $xmlElement[$relationType.'_'.$propertyName]['orphanRemoval']='true';
            }else {
                $xmlElement[$relationType.'_'.$propertyName]['orphanRemoval']='false';
            }
        }
        if($relationType != 'many-to-many') {
            $JoinColumn=[
                'name'=>ToolsService::camelCaseToSnakeCase($propertyName).'_id',
                'referenced-column-name'=>'id'
            ];
            if(isset($arguments['cascade']) && in_array('remove',$arguments['cascade'])) {
                $JoinColumn['on-delete'] = "CASCADE";
            }
            $JoinColumn['nullable'] = 'true';
            $xmlElement[$relationType.'_'.$propertyName]['join-column'] = $JoinColumn;
        } else {
            $JoinTable=[];
            if(isset($metadataProperty['constraint']['link']['name'])){
                $JoinTable['name'] = $metadataProperty['constraint']['link']['name'];
            }
            if(!empty($JoinTable)){
              $JoinTable['join-columns']= [
                'join-column'=> [
                    'name'=>ToolsService::camelCaseToSnakeCase($propertyName).'_id',
                    'referenced-column-name'=>'id'
                ]
              ];
              $JoinTable['inverse-join-columns']= [
                'join-column'=> [
                    'name'=>ToolsService::camelCaseToSnakeCase(ToolsService::getSimpleName($targetEntity)).'_id',
                    'referenced-column-name'=>'id'
                ]
              ];
              $xmlElement[$relationType.'_'.$propertyName]['join-table'] = $JoinTable;
            }
        }
    }
    
    private static function addSpecifiquePropertyName(array &$xmlElement,array $metadataProperty, string $propertyName):bool 
    {
        // if id field
        if($propertyName == 'id') {
            $xmlElement['id']= [
                'name' => $propertyName,
                'type' => 'integer',
                'generator' => [
                    'strategy'=>'AUTO'
                ]
            ];
            return true;
        } else if(isset($metadataProperty['constraint']['id'])){
            $xmlElement['id']= [
                'name' => $propertyName,
                'type' => $metadataProperty['type']
            ];
            return true;
        }
        return false;
    }
    private static function addExtends(array &$xmlElements,array $metadataEntity)
    {
        $xmlElements['inheritance-type']='JOINED';
        $xmlElements['discriminator-column']=[
            'name' => 'discr',
            'type' => 'string'
        ];
        if(!isset($xmlElements['discriminator-map'])) {
            $xmlElements['discriminator-map'] = [];
        }
        $nb = count($xmlElements['discriminator-map']);
        $xmlElements['discriminator-map']['discriminator-mapping_'.$nb]=[
            'value'=>strtolower($metadataEntity['className']),
            'class'=> $metadataEntity['namespace'].'\\'.$metadataEntity['className']
        ];
    }

    private static function addclassicField(array &$xmlElements,array $metadataProperty, string $propertyName)
    {
        
        $field = 'field_'.strtolower($propertyName);
        $xmlElements[$field]=[
            'name'=>$propertyName,
            'column'=>ToolsService::camelCaseToSnakeCase($propertyName)
        ];

        if(in_array($metadataProperty['type'],self::$specifiqueType)) {
            $type = match($metadataProperty['type']){
                Datetime::class => 'datetime'
            };
        }else if($metadataProperty['type'] == 'array'){ 
            $type = 'json';
        }else {
            $type= match($metadataProperty['type']){
                'int' => 'integer',
                'bool' => 'boolean',
                'float' => 'float',
                default => 'string'
            };
        }
        if($type == 'string') {
            if(isset($metadataProperty['constraint']['length'])){
                if($metadataProperty['constraint']['length'] > 255) {
                    $type = 'text';
                } else {
                    $xmlElements[$field]['length'] = $metadataProperty['constraint']['length'];
                }
            } else {
                $xmlElements[$field]['length'] = 255;
            }
        } elseif($type == 'float') {
            if(isset($metadataProperty['constraint']['precision'])){
                $xmlElements[$field]['precision'] = $metadataProperty['constraint']['precision'];
            } else {
                $xmlElements[$field]['precision']  = 4;
            }
            if(isset($metadataProperty['constraint']['scale'])){
                $xmlElements[$field]['scale']  = $metadataProperty['constraint']['scale'];
            } else {
                $xmlElements[$field]['scale']  = 2;
            }
        }
        if(isset($metadataProperty['constraint']['unique'])){
            if($metadataProperty['constraint']['unique']){
                $xmlElements[$field]['unique']='true';
            }else {
                $xmlElements[$field]['unique']='false';
            }
        }
        if(isset($metadataProperty['constraint']['nullable'])){
            if($metadataProperty['constraint']['nullable']){
                $xmlElements[$field]['nullable']='true';
            }else {
                $xmlElements[$field]['nullable']='false';
            }
        }
        $xmlElements[$field]['type'] = $type;
        if(isset($metadataProperty['constraint']['options'])){
            $xmlElements[$field]['options']=[];
            if(isset($metadataProperty['constraint']['options']['default'])){
                $xmlElements[$field]['options']['default'] = $metadataProperty['constraint']['options']['default'];
            }
        }
    }

    private static function replaceInterface(array $metadatasEntities,array $metadataEntity, array &$metadataProperty)
    {
        if(sizeof($metadatasEntities[$metadataProperty['type']]['implementedClass']) == 1) {
            $classimplemented = $metadatasEntities[$metadataProperty['type']]['implementedClass'][0];
            $metadataProperty['type'] = $classimplemented;
        } else {
            foreach($metadatasEntities[$metadataProperty['type']]['implementedClass'] as $implementedClass) {
                if(isset($metadatasEntities[$implementedClass]['parent']) && in_array($metadatasEntities[$implementedClass]['parent'],$metadatasEntities[$metadataProperty['type']]['implementedClass'])) {
                    continue;
                }
                $classimplementeds[]=$implementedClass;
            }
            if(sizeof($classimplementeds) > 1) {
                dump($metadataProperty['className']);
                dump($metadatasEntities[$metadataProperty['type']]);
                dd("multi interface not implemented in doctrine entity generator");
            } else {
                $metadataProperty['type'] = $classimplementeds[0];
            }
        }
    }
    private static function replaceArrayInterface(array $metadatasEntities,array $metadataEntity, array &$metadataProperty) 
    {
        if(sizeof($metadatasEntities[$metadataProperty['constraint']['containType']]['implementedClass']) == 1) {
            $classimplemented = $metadatasEntities[$metadataProperty['constraint']['containType']]['implementedClass'][0];
            $metadataProperty['constraint']['containType'] = $classimplemented;
        } else {
            foreach($metadatasEntities[$metadataProperty['constraint']['containType']]['implementedClass'] as $implementedClass) {
                if(isset($metadatasEntities[$implementedClass]['parent']) && in_array($metadatasEntities[$implementedClass]['parent'],$metadatasEntities[$metadataProperty['constraint']['containType']]['implementedClass'])) {
                    continue;
                }
                $classimplementeds[]=$implementedClass;
            }
            if(sizeof($classimplementeds) > 1) {
                dump($metadataProperty['className']);
                dump($metadatasEntities[$metadataProperty['constraint']['containType']]);
                dd("multi interface not implemented in doctrine entity generator");
            } else {
                $metadataProperty['constraint']['containType'] = $classimplementeds[0];
            }
        }
    }
    
    public static function getIgnoresNamespaces(): array {
        return [
            'Infra\Database\Migrations',
            'Infra\Database\Trait',
            'Infra\Database\Repository'
        ];
    }
}
