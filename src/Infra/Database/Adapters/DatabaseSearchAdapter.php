<?php

namespace Infra\Database\Adapters;

use Technical\DataManager\PaginatorManagerService;
use Technical\Common\Service\ToolsService;
use Technical\Common\Ports\SearchPort;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

class DatabaseSearchAdapter implements SearchPort
{
    public function __construct(
      private EntityManagerInterface $em,
      private PaginatorManagerService $pm
    )
    {
    }

    public function search($className,array $sorts = [],array $filters = [], $range = [])
    {
        $qb = $this->em->createQueryBuilder();
        $alias = ToolsService::getSimpleName($className);
        $qb->select($alias);
        $qb->from($className,$alias);
        $this->addQueryFilters($qb,$filters,$className);
        $this->addQuerySorts($qb,$sorts,$className);
        $this->addQueryRange($qb,$range);
        // for debug
        $sql = $qb->getQuery()->getSQL();
        $parameter = $qb->getQuery()->getParameters();
        $this->pm->setOrmPaginator(new Paginator($qb));
        return $this->pm;
    }

    public function addQueryRange(QueryBuilder &$qb ,array $range)
    {
      if(!empty($range)) {
        $max = ($range[1]-$range[0])+1;
        $qb->setFirstResult($range[0])->setMaxResults($max);
      }
    }

    public function addQueryFilters(QueryBuilder &$qb ,array $filters,string $className) 
    {
      if(!empty($filters)) {
        $where = $this->buildQuery($qb,$filters,$className);
        if(!is_null($where)){
          $qb->where($where);
        }
      }
    }

    private function buildQuery(QueryBuilder &$qb,array $filters,string $className)
    {
      $metadata = $this->em->getClassMetadata($className);
      $association = $metadata->getAssociationMappings();
      $alias = ToolsService::getSimpleName($className);
      $qb->addSelect($alias);
      $expr = [];
      foreach($filters as $filter => $filterValue) {
        if(array_key_exists($filter,$association)){
          $joinClass = $association[$filter]['targetEntity'];
          $jalias = ToolsService::getSimpleName($joinClass);
          $qb->leftJoin($alias.'.'.$filter,$jalias);
          if(is_string($filterValue) && preg_match('/.*\-.*\-.*\-.*\-.*/',$filterValue)){
            $filterValue = array('uuid' => $filterValue);
          }
          $expr[] = $this->buildQuery($qb,$filterValue,$joinClass);
        } else {
          switch ($filter) {
            case 'q':
              // is not implemented
              break;
            case 'geozone':
              $expr[] = $this->getExpGeozone($qb,$filterValue,$alias);
              $className::setLocalGeocode($filterValue['latitude'],$filterValue['longitude']);
              break;
            default:
              if(\is_array($filterValue)) {
                $expr[] = $qb->expr()->in($alias.'.'.$filter,':'.$alias.'_'.$filter);
              } else {
                $expr[] = $qb->expr()->eq($alias.'.'.$filter,':'.$alias.'_'.$filter);
              }
              $qb->setParameter(':'.$alias.'_'.$filter,$filterValue);
              break;
          }
        }
      }
      if(count($expr)>1){
        return $qb->expr()->andX(...$expr);
      } elseif(!empty($expr)) {
        return $expr[0];
      }
    }

    private function getExpGeozone(QueryBuilder &$qb ,array $geozone,$alias)
    {
      $sqlDistance = '(6378 * acos(cos(radians(' . $geozone['latitude'] . ')) * cos(radians('.$alias.'.latitude)) * cos(radians('.$alias.'.longitude) - radians(' . $geozone['longitude'] . ')) + sin(radians(' . $geozone['latitude'] . ')) * sin(radians('.$alias.'.latitude))))';
      $qb->setParameter(':'.$alias.'_distance',$geozone['distance']);
      $qb->addSelect($sqlDistance.'as HIDDEN '.$alias.'_distance');
      return $qb->expr()->lt($sqlDistance,':'.$alias.'_distance');
    }

    public function addQuerySorts(QueryBuilder &$qb ,array $sorts, string $className) 
    {
      if(!empty($sorts)){
        $metadata = $this->em->getClassMetadata($className);
        $association = $metadata->getAssociationMappings();
        $alias = ToolsService::getSimpleName($className);
        foreach ($sorts as $sort => $order) {
          if(array_key_exists($sort,$association)){
            $joinClass = $association[$sort]['targetEntity'];
            $jalias = ToolsService::getSimpleName($joinClass);
            $this->addQuerySorts($qb,$order,$joinClass);
          }
          if($sort == 'distance'){
            $qb->addOrderBy($alias.'_'.$sort,$order);
          } else {
            $qb->addOrderBy($alias.'.'.$sort,$order);
          }
        }
      }
    }
}
