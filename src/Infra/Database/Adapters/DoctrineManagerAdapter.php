<?php

namespace Infra\Database\Adapters;

use Technical\DataManager\LazyDataLoad\LazyDataLoadManagerInterface;
use Technical\DataManager\LazyDataLoad\LazyDataLoadInterface;
use Technical\DataManager\ExternalDataManagerPort;
use Technical\DataManager\Attributes\Persist;
use Technical\DataManager\Attributes\Ignore;
use Technical\Common\Service\ToolsService;
use ReflectionClass;
use Reflection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Proxy\Proxy;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class DoctrineManagerAdapter implements ExternalDataManagerPort
{
    public function __construct(
        private EntityManagerInterface $em
    ) {
    }

    public function save(object $entity) 
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function remove(object $entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
    
    public function getRepository(string $className) 
    {
        return $this->em->getRepository($className);
    }

    public static function applyEvent(string $eventName, object $event)
    {
        self::onPreFlush($event);
    }

    private static function onPreFlush(\Doctrine\ORM\Event\PreFlushEventArgs $event)
    {
        $em = $event->getEntityManager();
        foreach ($em->getUnitOfWork()->getScheduledEntityDeletions() as $object) {
            if (property_exists($object, "deletedAt")) {
                if ($object->deletedAt instanceof \Datetime) {
                    continue;
                } else {
                    $object->deletedAt = new \DateTime();
                    $em->persist($object);
                }
            }
        }
    }
}
