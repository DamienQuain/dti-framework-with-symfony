<?php

namespace Infra\File;

use Technical\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Gaufrette\Filesystem;
use Gaufrette\Adapter\Local;

class GaufretteSystemService implements FileSystemInterface
{
    private Filesystem $fileSystem;
    public function __construct(private ParameterBagInterface $parameter)
    {
        $upload_path=$parameter->get('upload_path');
        $adater = new Local($upload_path);
        $this->fileSystem = new Filesystem($adater);
    }

    public function upload(string $filename, string $content) 
    {
        $this->fileSystem->write($filename,$content,true);
    }

    public function delete(string $filename)
    {
        $this->fileSystem->delete($filename);
    }
}