<?php

namespace Technical\Common\Adapters;

use Technical\Common\Exceptions\ValidatorExceptions;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Domain\Common\Ports\ValidatorPort;

class ValidatorAdapter implements ValidatorPort
{
    public function __construct(
        private ValidatorInterface $validator
    )
    {
        
    }
    public function validate(object $object,array|null $constraints=null, array|null $groups=null)
    {
        $validationErrors = $this->validator->validate($object,$constraints,$groups);
        if(count($validationErrors) > 0) {
            throw new ValidatorExceptions($validationErrors);
        }
    }
}
