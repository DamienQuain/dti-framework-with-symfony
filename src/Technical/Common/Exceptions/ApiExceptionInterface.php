<?php
namespace Technical\Common\Exceptions;

interface ApiExceptionInterface
{
  public function getHttpCode(): int;
  public function getContentForJson(): mixed;
}