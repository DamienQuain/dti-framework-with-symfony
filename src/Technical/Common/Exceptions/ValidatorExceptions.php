<?php
namespace Technical\Common\Exceptions;

use Throwable;
use Symfony\Component\HttpFoundation\Response;
use Exception;

class ValidatorExceptions extends Exception implements ApiExceptionInterface
{
    private object $validationErrors;
    public function __construct(object $validationErrors,?Throwable $previous = null )
    {
        $this->validationErrors = $validationErrors;
        parent::__construct("validation Error",400,$previous);
    }

    public function getValidationErrors(): object 
    {
        return $this->validationErrors;
    }

    public function getContentForJson(): mixed
    {
        return $this->getValidationErrors();
    }

    public function getHttpCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}