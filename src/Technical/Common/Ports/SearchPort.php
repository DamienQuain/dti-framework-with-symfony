<?php

namespace Technical\Common\Ports;

interface SearchPort
{
    public function search(string $className, array $sorts = [],array $filters=[], array $range=[]);
}