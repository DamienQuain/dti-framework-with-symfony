<?php
namespace Technical\Common\Traits;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

trait CrudTrait
{
    private static $baseClass;

    #[Route(methods:['POST'])]
    public function create(Request $request): Response
    {
        return $this->crudService->create(
            $request,
            self::$baseClass,
            [
                'groups' => self::$baseClass::GROUP_CREATE
            ],
            [
                'groups' => self::$baseClass::GROUP_READ
            ]
        );
    }

    #[Route('/{uuid}',methods:['GET'])]
    public function read(Request $request, string $uuid) : Response
    {
        return $this->crudService->read(
            $request,
            self::$baseClass,
            $uuid,
            [
                'groups' => self::$baseClass::GROUP_READ
            ]
        );
    }

    #[Route('/{uuid}',methods:['PUT'])]
    public function update(Request $request, string $uuid): Response
    {
        return $this->crudService->update(
            $request,
            self::$baseClass,
            $uuid,
            [
                'groups' => self::$baseClass::GROUP_UPDATE,
            ]
        );
    }

    #[Route('/{uuid}',methods:['DELETE'])]
    public function delete(Request $request, string $uuid) : Response
    {
        return $this->crudService->delete(
            $request,
            self::$baseClass,
            $uuid
        );
    }

    #[Route(methods:['GET'])]
    public function readAll(Request $request) : Response
    {
        return $this->crudService->readAll(
            $request,
            self::$baseClass,
            [
                'groups' => self::$baseClass::GROUP_MINIMAL
            ]
        );
    }
}
