<?php

namespace Technical\Common\Service;

use ReflectionProperty;
use ReflectionClass;

abstract class ToolsService
{
    public static function getSimpleName($globalName)
    {
        $array = explode('\\', $globalName);
        return end($array);
    }

    public static function endsWith($string, $endString)
    {
        $len = strlen($endString);
        if ($len == 0) {
            return true;
        }
        return (substr($string, -$len) === $endString);
    }


    public static function getAllReflexionProperties(ReflectionClass $reflexionClass): array
    {
        $reflexionProperties = $reflexionClass->getProperties();
        while($reflexionClass = $reflexionClass->getParentClass()) {
            $reflexionProperties = [...$reflexionProperties,...$reflexionClass->getProperties()];
        }
        return $reflexionProperties;
    }

    public static function hasReflexionProperty(ReflectionClass $reflexionClass,string $propertyName):bool
    {
        if($reflexionClass->hasProperty($propertyName)) {
            return true;
        } else {
             while($reflexionClass = $reflexionClass->getParentClass()) {
                 if($reflexionClass->hasProperty($propertyName)) {
                     return true;
                 }
             }
        }
        return false;
    }

    public static function getReflexionProperty(ReflectionClass $reflexionClass,string $propertyName):?ReflectionProperty
    {
       if($reflexionClass->hasProperty($propertyName)) {
           return $reflexionClass->getProperty($propertyName);
       } else {
            while($reflexionClass = $reflexionClass->getParentClass()) {
                if($reflexionClass->hasProperty($propertyName)) {
                    return $reflexionClass->getProperty($propertyName);
                }
            }
       }
       return null;
    }

    public static function camelCaseToSnakeCase($input): string 
    {
        $pattern = '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!';
        preg_match_all($pattern, $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ?
                strtolower($match) :
                lcfirst($match);
        }
        return implode('_', $ret);
    }
}