<?php

namespace Technical\Common\Service;

use Technical\Security\SecurityPolicy;
use Technical\DataManager\DataManagerService;
use Technical\Common\Ports\SearchPort;
use Technical\Common\Exceptions\ApiExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Domain\Common\Ports\CUDPort;

class CrudService
{

    public function __construct(
        private DataManagerService $dm,
        private SerializerInterface $serializer, 
        private SearchPort $search, 
        private Security $security,
        private CUDPort $domainService)
    {

    }

    public function setDomainService(CUDPort $domainService) 
    {
        $this->domainService = $domainService;
        return $this;
    }

    /**
     * create entity
     *
     * @param Request $request
     * @param string $className
     * @param array $writecontext
     * @param array $readcontext
     * @return Response
     */
    public function create(Request $request,string $className,array $writecontext, array $readcontext) : Response
    {
        $data = $request->getContent();
        $object = $this->serializer->deserialize($data,
                                                $className,
                                                'json',
                                                $writecontext
                                            );
        if(!$this->security->isGranted(SecurityPolicy::PERMISSION_CREATE,$object)){
            throw new AccessDeniedException();
        }
        
        try{
            $this->domainService->create($object,$writecontext);
        } catch( ApiExceptionInterface $apiException){
            return new Response(
                $this->serializer->serialize($apiException->getContentForJson(),'json'),
                $apiException->getHttpCode()
            );
        } 
        
        
        return new Response(
            $this->serializer->serialize(
                $object,
                'json',
                $readcontext
            ), 
            Response::HTTP_CREATED
        );
    }

    /**
     * read entity
     *
     * @param Request $request
     * @param string $className
     * @param string $uuid
     * @param array $readcontext
     * @return Response
     */
    public function read(Request $request,string $className, string $uuid, array $readcontext) : Response
    {
        $object = $this->dm->load($className,$uuid);
        if(is_null($object)) {
            return new Response(
                $this->serializer->serialize(
                    ['message'=>'error this entity doesn\'t exist'],
                    'json'
                ),
                Response::HTTP_BAD_REQUEST
            );
        }
        if(!$this->security->isGranted(SecurityPolicy::PERMISSION_READ,$object)){
            throw new AccessDeniedException();
        }
        return new Response(
            $this->serializer->serialize(
                $object,
                'json',
                $readcontext
            ),
            Response::HTTP_OK,
        );
    }


    /**
     * read all same entity with pagination
     *
     * @param Request $request
     * @param string $className
     * @param array $readcontext
     * @return Response
     */
    public function readAll(Request $request,string $className, array $readcontext) : Response
    {        
        $data = json_decode($request->getContent());
        if(isset($data['range'])){
            $range = $data['range'];
        }elseif($request->query->get('range')){
            $range = json_decode($request->query->get('range'),true);
        }
        if(isset($data['filters'])){
            $filters = $data['filters'];
        }elseif($request->query->get('filters')){
            $filters = json_decode($request->query->get('filters'),true);
        }
        if(isset($data['sorts'])){
            $sorts = $data['sorts'];
        }elseif($request->query->get('sorts')){
            $sorts = json_decode($request->query->get('sorts'),true);
        }
        
        // set default value if not isset
        if(!isset($sorts)) {
            $sorts = [];
        }
        if(!isset($filters)) {
            $filters = [];
        }
        if(!isset($range)) {
            $range = [0,9];
        }
        $objectList = $this->search->search($className,$sorts,$filters,$range);
        $total = count($objectList);
        $minClassName = strtolower(ToolsService::getSimpleName($className));
        return new Response(
            $this->serializer->serialize(
            $objectList,
            'json',
            $readcontext),
            Response::HTTP_OK,
            ['Content-Range' => "$minClassName $range[0]-$range[1]/$total"]
        );
    }

    /**
     * update entity
     *
     * @param Request $request
     * @param string $className
     * @param array $writecontext
     * @return Response
     */
    public function update(Request $request,string $className,string $uuid,array $writecontext) : Response
    {
        $object = $this->dm->load($className,$uuid);
        if(is_null($object)) {
            return new Response(
                $this->serializer->serialize(
                    ['message'=>'error this entity doesn\'t exist'],
                    'json'
                ),
                Response::HTTP_BAD_REQUEST
            );
        }
        if(!$this->security->isGranted(SecurityPolicy::PERMISSION_UPDATE,$object)){
            throw new AccessDeniedException();
        }
        $writecontext = array_merge([
            AbstractNormalizer::OBJECT_TO_POPULATE => $object,
        ],$writecontext);
        $object = $this->serializer->deserialize($request->getContent(),
                                        $className,
                                        'json',
                                        $writecontext
                                    );
        try {
            $this->domainService->update($object,$writecontext);
        } catch( ApiExceptionInterface $apiException){
            return new Response(
                $this->serializer->serialize($apiException->getContentForJson(),'json'),
                $apiException->getHttpCode()
            );
        } 
        
        return new Response(
            $this->serializer->serialize(["success" => true],'json'),
            Response::HTTP_OK
        );
    }

    /**
     * delete entity
     *
     * @param Request $request
     * @param string $className
     * @param string $uuid
     * @return Response
     */
    public function delete(Request $request,string $className, string $uuid) : Response
    {
        $object = $this->dm->load($className,$uuid);
        if(is_null($object)) {
            return new Response(
                $this->serializer->serialize(
                    ['message'=>'error this entity doesn\'t exist'],
                    'json'
                ),
                Response::HTTP_BAD_REQUEST
            );
        }
        if(!$this->security->isGranted(SecurityPolicy::PERMISSION_DELETE,$object)){
            throw new AccessDeniedException();
        }
        $this->domainService->delete($object);
        return new Response(
            $this->serializer->serialize(["success" => true],'json'),
            Response::HTTP_OK
        );
    }





}
