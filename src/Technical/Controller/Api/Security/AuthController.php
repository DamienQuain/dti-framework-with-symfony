<?php

namespace Technical\Controller\Api\Security;

use Technical\Security\TechnicalUser;
use Technical\Security\RefreshToken;
use Technical\Security\AuthService;
use Technical\DataManager\DataManagerService;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
Use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Domain\User\User;

#[Route("/auth")]
class AuthController extends AbstractController
{
    public function __construct(
        private DataManagerService $dm,
        private SerializerInterface $serializer,
        private AuthService $authService,

    )
    {
    }

    #[Route('/register',methods:['POST'])]
    public function register(Request $request)
    {
        $techUser = $this->serializer->deserialize(
            $request->getContent(),
            TechnicalUser::class,
            'json',
            [
                'groups' => TechnicalUser::GROUP_CREATE
            ]
        );
        $userExist = $this->dm->getRepository(TechnicalUser::class)->findOneBy(['email'=>$techUser->getEmail()]);
        if(!isset($userExist)) {
            $data = json_decode($request->getContent());
            $techUser->setPassword($this->authService->hashPassword($techUser,$data->password)); 
            $user = $this->serializer->deserialize(
                $request->getContent(),
                User::class,
                'json',
                [
                    'groups' => User::GROUP_CREATE
                ]
            );
            $user->setTechnicalUser($techUser);
            /*$validationErrors = $validator->validate($user);
            if(count($validationErrors) > 0) {
                return new Response(
                    $serializer->serialize($validationErrors,'json'),
                    Response::HTTP_BAD_REQUEST
                );
            }*/
            $this->dm->save($user);
            $payload = [
                "uuid" => $techUser->getUuid()
            ];
            $jwt = $this->authService->createToken($payload,$techUser);
            return $this->json([
                'message' => 'success!',
                'token' => $jwt,
            ],Response::HTTP_CREATED);
        } else {
            return $this->json([
                "success" => false,
                "message" => 'user already exist'
            ],Response::HTTP_CONFLICT);
        }
    }


    #[Route('/login',methods:['POST'])]
    public function login(Request $request)
    {
        $data = json_decode($request->getContent());
        if(isset($data->username)){
            $data->email = $data->username;
        }
        $techUser = $this->dm->getRepository(TechnicalUser::class)->findOneBy([
                'email'=>$data->email,
        ]);
        if (!$techUser || !$this->authService->verifyPassword($techUser, $data->password)) {
                return $this->json([
                    'message' => 'email or password is wrong.',
                ],Response::HTTP_UNAUTHORIZED);
        }
        $payload = [
            "uuid" => $techUser->getUuid(),
        ];
        $jwt = $this->authService->createToken($payload,$techUser);
        return $this->json([
            'message' => 'success!',
            'token' => $jwt,
        ]);
    }


    #[Route('/refresh',methods:['POST'])]
    public function refresh(Request $request)
    {
        // check the token is signed even if is expired
        if($request->headers->has('Authorization')) {
            $token = $request->headers->get('Authorization');
            $token = str_replace('Bearer ', '', $token);
        } else {
            $data = json_decode($request->getContent());
            $token = str_replace('Bearer ', '', $data->token);
        }
        try {
            $payload = (array) JWT::decode(
                $token, 
                $this->getParameter('jwt_secret'),
                ['HS256']
            );
        } catch(\Firebase\JWT\ExpiredException $e){
            JWT::$leeway = 720000;
            $payload = (array) JWT::decode(
                $token, 
                $this->getParameter('jwt_secret'),
                ['HS256']
            );
        } catch(\Exception $e) {
            return $this->json([
                "success" => false,
                "message" => $e->getMessage()
            ],Response::HTTP_UNAUTHORIZED);
        }
        $timestamp = \time();
        // check if the expRefresh is not exceed the now timestamp
        if($timestamp <= $payload["expRefresh"]) {
            $signedToken = explode('.',$token)[2];
            $tokenExist = $this->dm->getRepository(RefreshToken::class)->findOneBy(['token'=>$signedToken]);
            if(!isset($tokenExist)) {
                return $this->json([
                    "success" => false,
                    "message" => 'refresh token is already used'
                ],Response::HTTP_UNAUTHORIZED);
            }
            $this->dm->remove($tokenExist);
            $jwt = $this->authService->createToken($payload,$this->getUser());
            return $this->json([
                'message' => 'success!',
                'token' => $jwt,
            ]);
        } else {
            return $this->json([
                "success" => false,
                "message" => 'the refresh token is expired'
            ],401);
        }
    }

    #[Route('/change-password',methods:['POST'])]
    public function changePassword(Request $request)
    {
        $data = json_decode($request->getContent());
        if(isset($data->currentPassword) && isset($data->newPassword)){
            $techUser = $this->getUser();
            if (!$techUser || !$this->authService->verifyPassword($techUser, $data->currentPassword)) {
                return $this->json([
                    'message' => 'password is wrong.',
                ],Response::HTTP_UNAUTHORIZED);
            } else {
                $techUser->setPassword($this->authService->hashPassword($techUser,$data->newPassword));
                $this->dm->save($techUser);
                return $this->json(['success' => true]);
            }
        } else {
            return $this->json(['message' => 'currentPassword and newPassword is required'],Response::HTTP_BAD_REQUEST);
        }
    }
}
