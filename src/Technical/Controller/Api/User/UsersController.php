<?php

namespace Technical\Controller\Api\User;

use Technical\Security\TechnicalRole;
use Technical\DataManager\DataManagerService;
use Technical\Common\Traits\CrudTrait;
use Technical\Common\Service\CrudService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Domain\User\User;

#[Route("/users")]
class UsersController extends AbstractController
{
    use CrudTrait;
    public function __construct(
        private CrudService $crudService,
        private DataManagerService $dm
    )
    {
        self::$baseClass = User::class;
    }

    #[Route('/me',priority:1,methods:['GET'])]
    public function me(Request $request)
    {
        return $this->json(
            $this->getUser()->getUser(),
            Response::HTTP_OK,
            [],
            [
                'groups'=> self::$baseClass::GROUP_READ,
            ]
        );
    }

}