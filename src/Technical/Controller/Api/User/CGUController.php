<?php

namespace Technical\Controller\Api\User;

use Technical\DataManager\DataManagerService;
use Technical\Common\Traits\CrudTrait;
use Technical\Common\Service\CrudService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Domain\User\CGUService;
use Domain\User\CGU;

#[Route("/cgu")]
class CGUController extends AbstractController
{
    use CrudTrait;
    public function __construct(
        private CrudService $crudService,
        private DataManagerService $dm,
        private CGUService $cGUService
    )
    {  
        self::$baseClass = CGU::class;
        $this->crudService->setDomainService($cGUService);
    }
    
    #[Route('/version/{numVersion}',methods:['GET'])]
    public function readWithVersion(Request $request, string $numVersion) : Response
    {
        $cgu = null;
        $type = $request->get('type');
        if(isset($type))
        {
            if($numVersion == 'last')
            {
                $cgu = $this->dm->getRepository(CGU::class)->findLast($type);
            } else {
                $cgu = $this->dm->getRepository(CGU::class)->findOneBy(['type'=>$type,'version'=>$numVersion]);
            }
        }
        return $this->json(
            $cgu,
            Response::HTTP_OK,
            [],
            [
            'groups' => CGU::GROUP_READ
            ]
        );
    }
}
