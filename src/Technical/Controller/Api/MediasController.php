<?php

namespace Technical\Controller\Api;

use Technical\File\MediaService;
use Technical\File\Media;
use Technical\File\FileManagerService;
use Technical\DataManager\DataManagerService;
use Technical\Common\Service\CrudService;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route("/medias")]
class MediasController extends AbstractController
{
    public function __construct(
        private CrudService $crudService,
        private MediaService $mediaService
    )
    {  
        $crudService->setDomainService($mediaService);
    }

    #[Route(methods:['POST'])]
    public function upload(Request $request, ValidatorInterface $validator, DataManagerService $dm, FileManagerService $fm)
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }
        $media = new Media();
        $media->setFile($uploadedFile);
        $fm->upload($media);
        $validationErrors = $validator->validate($media, null, [Media::UPDATE]);
        if(count($validationErrors) > 0) {
           return new Response(
               $this->serializer->serialize($validationErrors,'json'),
               Response::HTTP_BAD_REQUEST
            );
        }
        $dm->save($media);
        return $this->json(
            $media,
            Response::HTTP_CREATED,
            [],
            [
                'groups' => Media::GROUP_MINIMAL
            ]
        );
    }

    #[Route('/{uuid}',methods:['POST'])]
    public function update(Request $request, ValidatorInterface $validator, string $uuid, DataManagerService $dm, FileManagerService $fm) : Response
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $media = $dm->load(Media::class,$uuid);
        $fm->delete($media);
        $media->setFile($uploadedFile);
        $fm->upload($media);

        $validationErrors = $validator->validate($media, null, [Media::UPDATE]);
        if(count($validationErrors) > 0) {
           return new Response(
               $this->serializer->serialize($validationErrors,'json'),
               Response::HTTP_BAD_REQUEST
            );
        }
        $dm->save($media);
        return $this->json(
            $media,
            Response::HTTP_OK,
            [],
            [
                'groups' => Media::GROUP_READ
            ]
        );
    }

    #[Route('/{uuid}',methods:['GET'])]
    public function read(Request $request, string $uuid) : Response
    {
        return $this->crudService->read(
            $request,
            Media::class,
            $uuid,
            ['groups' => Media::GROUP_READ
            ]
        );
    }

    #[Route('/{uuid}',methods:['DELETE'])]
    public function delete(Request $request, string $uuid) : Response
    {
        return $this->crudService->delete(
            $request,
            Media::class,
            $uuid
        );
    }

    #[Route(methods:['GET'])]
    public function readAll(Request $request) : Response
    {
        return $this->crudService->readAll(
            $request,
            Media::class,
            ['groups' => Media::GROUP_MINIMAL
            ]
        );
    }
}
