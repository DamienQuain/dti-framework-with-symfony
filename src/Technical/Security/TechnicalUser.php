<?php

namespace Technical\Security;

use Technical\DataManager\Attributes as DM;
use Technical\Common\Service\ToolsService;
use Symfony\Component\Serializer\Annotation as Serialize;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Domain\User\User;
use Domain\User\TechnicalUserInterface;
use Domain\Common\Traits\IdTrait;

#[DM\Persist]
class TechnicalUser implements PasswordAuthenticatedUserInterface,TechnicalUserInterface,UserInterface
{
    use IdTrait {
        IdTrait::__construct as private __idConstruct;
    }
    private array $roles = [];
    
    #[DM\Persist(length:180, unique: true)]
    private string $email;
    
    private string $password;
    
    #[DM\Persist(cascade:['persist','remove'])]
    #[DM\Link(type: 'OneToOne', inversedBy:'technicalUser')]
    private User $user;

    public function __construct(
     
    ) 
    {
      $this->__idConstruct();
    }
    
    /**
     *
     * @return string|null
     */
    #[Serialize\Groups([self::MINIMAL])]
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set Email
     * @param string $email
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * Set User Password
     * @param string $password
     * @return self
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }
  
    #[Serialize\Groups([self::READ])]
    public function getRoles(): array
    {
        $roles = $this->roles;
        if(isset($this->user)) {
            $rolesUser = $this->user->getRoles();
            $rolesNames=[];
            foreach ($rolesUser as $roleUser) {
                $rolesNames[] = $roleUser->getTechnicalRole()->getName();
            }
            $rolesNames[]='ROLE_'.strtoupper(ToolsService::getSimpleName($this->user::class));
            $roles=[...$roles,...$rolesNames];
        }
        // guarantee every user at least has ROLE_USER
        $roles[] = TechnicalRole::USER;
        return array_unique($roles);
    }

    /**
     * Add new role
     *
     * @param string $role
     * @return void
     */
    public function addRole(string $role): self
    {
        $this->roles[]=$role;

        return $this;
    }

    public function removeRole(string $role): self
    {
        if (($key = array_search($role, $this->roles)) !== false) {
            unset($this->roles[$key]);
        }
        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    } 

    #[Serialize\Groups([self::READ])]
    public function getUser(): ?User
    {
        if(isset($this->user)) {
            return $this->user;
        } else {
            return null;
        }
    }

    #[Serialize\Groups([self::UPDATE])]
    public function setUser(?User $user): self
    {
        if(!isset($this->user)) {
            $this->user = $user;
        }
        return $this;
    }


    public function eraseCredentials()
    {
        
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }
}
