<?php
namespace Technical\Security;

use Technical\DataManager\DataManagerService;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Firebase\JWT\JWT;

class JwtAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private DataManagerService $dm, 
        private ContainerBagInterface $params
    )
    {}

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [ 
            'message' => 'Authentication Required'
        ];
        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supports(Request $request): ?bool
    {
      return $request->headers->has('Authorization'); 
    }

    public function authenticate(Request $request): Passport
    {
        try {
            $credentials = str_replace('Bearer ', '', $request->headers->get('Authorization'));
            $jwt = (array) JWT::decode(
                              $credentials, 
                              $this->params->get('jwt_secret'),
                              ['HS256']
                            );
            return new SelfValidatingPassport(
                new UserBadge($jwt['uuid'], function ($userIdentifier) {
                    return $this->dm->load(TechnicalUser::class,$userIdentifier);
                }));
        }catch (\Exception $exception) {
                throw new AuthenticationException($exception->getMessage());
        }
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse([
            'message' => $exception->getMessage()
        ], Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): ?Response
    {
        return null;
    }
}
