<?php

namespace Technical\Security\Voter;

use Technical\Security\TechnicalRole;
use Technical\Security\SecurityPolicyService;
use Technical\Security\SecurityPolicy;
use Technical\DataManager\DataManagerService;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class TechnicalRoleVoter extends Voter
{
    public function __construct(
        private DataManagerService $dm,
        private Security $security 
    )
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        $attributeSupported = [
            SecurityPolicy::PERMISSION_CREATE,
            SecurityPolicy::PERMISSION_READ,
            SecurityPolicy::PERMISSION_UPDATE,
            SecurityPolicy::PERMISSION_DELETE
        ];

        if(in_array($attribute, $attributeSupported)){
            if(\is_object($subject)){
                return true;
            }
        }
        return false;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $baseGranted = $this->security->isGranted(SecurityPolicyService::getBaseRoles($subject::class,$attribute));
        if(!$baseGranted){
            return false;
        }
        $user = $token->getUser();
        if(empty($user)){
            return $baseGranted;
        }
        $policy = SecurityPolicyService::mergePolicies($this->getAllSecurityPolicies($user));
        if(!is_null($policy)) {
            return SecurityPolicyService::applyPolicy($user,$subject,$attribute,$policy);
        } else {
            return true;
        }
    }

    private function getAllSecurityPolicies(UserInterface $user): array
    {
        $policies = [];
        $rolesName = $user->getRoles();
        $roles = $this->dm->getRepository(TechnicalRole::class)->findBy(["name"=>$rolesName]);
        foreach($roles as $role){
            $policies[] = $role->getTechnicalRole()->getSecurityPolicy();
        }
        return $policies;
    }
}
