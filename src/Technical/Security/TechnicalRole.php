<?php

namespace Technical\Security;

use Technical\DataManager\Attributes as DM;
use Symfony\Component\Serializer\Annotation as Serialize;
use Domain\User\TechnicalRoleInterface;
use Domain\User\Role;
use Domain\Common\Traits\IdTrait;


#[DM\Persist]
class TechnicalRole implements TechnicalRoleInterface
{
    use IdTrait;
    
    #[DM\Persist(unique: true)]
    private string $name;

    private SecurityPolicy $securityPolicy;

    #[DM\Persist(cascade:['persist','remove'])]
    #[DM\Link(type: 'OneToOne', inversedBy:'technicalRole')]
    private Role $role;
    /**
     * get name
     * @return string|null
     */
    #[Serialize\Groups([self::MINIMAL])]
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * set name
     * @param string $name
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return SecurityPolicy
     */
    #[Serialize\Groups([self::READ])]
    public function getSecurityPolicy(): SecurityPolicy
    {
        return $this->securityPolicy;
    }

    /**
     * @param SecurityPolicy $securityPolicy
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setSecurityPolicy(SecurityPolicy $securityPolicy): self
    {
        $this->securityPolicy = $securityPolicy;

        return $this;
    }

     /**
     * get Role 
     *
     * @return Role|null
     */
    #[Serialize\Groups([self::READ])]
    public function getRole(): ?Role
    {
        return $this->role;
    }

    /**
     * set Role
     * @param Role|null $Role
     * @return self
     */
    #[Serialize\Groups([self::UPDATE])]
    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }
}
