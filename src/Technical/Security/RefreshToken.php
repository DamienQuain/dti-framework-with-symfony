<?php

namespace Technical\Security;

use Technical\DataManager\Attributes as DM;
use DateTime;


#[DM\Persist]
class RefreshToken
{
    #[DM\Persist(id: true)]
    private string $token;

    private DateTime $createAt;

    public function __construct()
    {
        $this->createAt = new \DateTime();
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }
}
