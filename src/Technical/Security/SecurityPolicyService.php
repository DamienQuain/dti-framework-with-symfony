<?php

namespace Technical\Security;

use Technical\Security\Attributes\BaseRole;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use ReflectionClass;

class SecurityPolicyService
{
    public static function getBaseRoles(string $className, string $permission) 
    {
        $reflexionClass = new ReflectionClass($className);
        if(!empty($reflexionClass->getAttributes(BaseRole::class)))
        {
            $baseRole = $reflexionClass->getAttributes(BaseRole::class)[0]->getArguments();
            $permission = explode(':',$permission)[1];
            if(isset($baseRole[$permission])) {
                return $baseRole[$permission];
            }
        }
        return AuthenticatedVoter::PUBLIC_ACCESS;
    }
    public static function mergePolicies( array $policies): ?SecurityPolicy
    {
        if(empty($policies)){
            return null;
        }
        return $policies[0];
    }

    private static function isLink(UserInterface $user,object $object,array $blackListedField = [])
    {
        return true;
    }

    public static function applyPolicy(UserInterface $user,object $object,string $permission,SecurityPolicy $policy):bool
    {
        $rule = $policy->getRule($object::class,$permission);
        $result = match($rule){
            true,false => $rule,
            'isLink' => self::isLink($user,$object),
            default => self::resolveRule($user,$object,$rule)
        };
        return $result;
    }

    private static function resolveRule(UserInterface $user, object $object, array|string $rule):bool
    {
        return true;
    }
}
