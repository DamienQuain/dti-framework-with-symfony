<?php

namespace Technical\Security;

use Technical\DataManager\DataManagerService;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Firebase\JWT\JWT;

class AuthService
{
    public function __construct(
        private ContainerBagInterface $params,
        private UserPasswordHasherInterface $hasher,
        private DataManagerService $dm
    )
    {
    }

    public function hashPassword(TechnicalUser $user, $password) 
    {
        return $this->hasher->hashPassword($user,$password);
    }

    public function verifyPassword(TechnicalUser $user, string $password): bool
    {
        return $this->hasher->isPasswordValid($user, $password);
    }
    
    /**
     *  create the token for authentification
     *  @return string $jwt
     */
    public function createToken(array $payload,TechnicalUser $techUser)
    {
        $payload["iat"] = (new \DateTime())->getTimestamp();
        $payload["exp"] = (new \DateTime())->modify('+'.$this->params->get('jwt_exp'))->getTimestamp();
        $payload["expRefresh"] = (new \DateTime())->modify('+'.$this->params->get("jwt_exp_refresh"))->getTimestamp();
        $jwt = JWT::encode($payload, $this->params->get('jwt_secret'), 'HS256');
        // save refresh Token
        $signedToken = explode('.',$jwt)[2];
        $refreshToken = new RefreshToken();
        $this->dm->save($refreshToken->setToken($signedToken));
        return $jwt;
    }
}
