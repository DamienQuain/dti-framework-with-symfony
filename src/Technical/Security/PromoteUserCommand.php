<?php

namespace Technical\Security;

use Technical\DataManager\DataManagerService;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Domain\User\User;

class PromoteUserCommand extends Command
{
    protected static $defaultName = 'platform:user:promote';

    public function __construct(
        private DataManagerService $dm)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Promote a user by adding him a new roles.')
            ->addArgument('email', InputArgument::REQUIRED, 'Email address of the user you want to promote.')
            ->addArgument('role', InputArgument::REQUIRED, 'The roles you want to add to the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $role = $input->getArgument('role');

        $userRepository = $this->dm->getRepository(TechnicalUser::class);
        $user = $userRepository->findOneByEmail($email);

        if ($user) {
            $user->addRole($role);
            $this->dm->save($user);

            $io->success('The roles has been successfully added to the user.');
        } else {
            $io->error('There is no user with that email address.');
        }

        return 0;
    }
}
