<?php
namespace Technical\Security\Attributes;

use Attribute;

#[Attribute]
class BaseRole
{
    public string $create;
    public string $read;
    public string $update;
    public string $delete;
}