<?php

namespace Technical\Security;
use Technical\Serializer\Attributes as SE;
use Technical\DataManager\Attributes as DM;

#[DM\Persist(format:'json')]
#[SE\Serialize]
class SecurityPolicy
{
    const BLACKFIELD = 'blackfields';
    const All = 'all';
    const DEFAULT = 'default';

    const PERMISSION_CREATE = 'permission:create';
    const PERMISSION_UPDATE = 'permission:update';
    const PERMISSION_READ = 'permission:read';
    const PERMISSION_DELETE = 'permission:delete';

    /**
     * @var $rules;
     */
    private array $rules;

    public function __construct()
    {
        $this->rules = [
            self::DEFAULT => [
                self::PERMISSION_CREATE => false,
                self::PERMISSION_UPDATE => false,
                self::PERMISSION_READ => false,
                self::PERMISSION_DELETE => false,
                self::BLACKFIELD => []
            ]
        ];
    }

    public function getRule(string $classname,string $permission): string|bool
    {
        if(isset($this->rules[self::All]) && isset($this->rules[self::All][$permission]))
        {
            return $this->rules[self::All][$permission];
        }
        if(!isset($this->rules[$classname]) || !isset($this->rules[$classname][$permission]) ){
            return $this->rules[self::DEFAULT][$permission];
        } 
        return $this->rules[$classname][$permission];
    }

    public function addRule(string $classname, string $permission, string|bool $condition)
    {
        if(!isset($this->rules[$classname])){
            $this->rules[$classname] = [];
        }
        $this->rules[$classname][$permission]=$condition;
    }

    public function getRuleBlackListedFields(string $classname)
    {
        if(!isset($this->rules[$classname]) || !isset($this->rules[$classname][self::BLACKFIELD]) ){
            return $this->rules[self::DEFAULT][self::BLACKFIELD];
        } 
        return $this->rules[$classname][self::BLACKFIELD];
    }

    public function addRuleBlackListedFields(string $classname,array $fields)
    {
        if(!isset($this->rules[$classname])){
            $this->rules[$classname] = [];
        }
        $this->rules[$classname][self::BLACKFIELD]=$fields;
    }

    public function getRules(array $rules)
    {
        return $this->rules;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;

        return $this;
    }

}
