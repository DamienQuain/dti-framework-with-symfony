<?php

namespace Technical\DataManager;

interface ExternalDataManagerPort
{
    public function save(object $object);
    public function remove(object $object);
    public function getRepository(string $className);

    public static function applyEvent(string $eventName,object $event);
}
