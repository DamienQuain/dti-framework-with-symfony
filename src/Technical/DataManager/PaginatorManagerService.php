<?php
namespace Technical\DataManager;

use IteratorAggregate;
use Countable;

class PaginatorManagerService implements Countable,IteratorAggregate
{
    private object $ormPaginator;
    public function __construct( 
        private ExternalDataManagerPort $edm
    )
    {
        
    }

    public function setOrmPaginator(object $ormPaginator) 
    {
        $this->ormPaginator = $ormPaginator;
    }
    
    public function count()
    {
        return $this->ormPaginator->count();
    }

    public function getIterator()
    {
        $itterator = $this->ormPaginator->getIterator();
        return $itterator;
    }
}