<?php
namespace Technical\DataManager;


class RepositoryManagerService
{
   private object $repository;
   public function __construct(
       string $className,
       private ExternalDataManagerPort $edm
    ) 
   {
       $this->repository = $edm->getRepository($className);
   }

   public function __call ( string $name , array $arguments ) : mixed
   {
       $result = $this->repository->$name(...$arguments);
       return $result;
   } 
}

