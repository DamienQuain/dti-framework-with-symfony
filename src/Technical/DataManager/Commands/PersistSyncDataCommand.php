<?php

namespace Technical\DataManager\Commands;

use Technical\DataManager\SyncPersistEntityService;
use Technical\DataManager\SyncPersistDataService;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Command\Command;
use Infra\Database\Adapters\DoctrineDataSyncAdapter;

class PersistSyncDataCommand extends Command
{
    protected static $defaultName = 'technical:persist:sync';

    public function __construct()
    {
      parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Sync the persist entity')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->info("This command Sync Entity");
        SyncPersistDataService::sync(new DoctrineDataSyncAdapter);
        return 0;
    }
}