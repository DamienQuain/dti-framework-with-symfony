<?php

namespace Technical\DataManager;

use Technical\File\FileTools;
use Technical\DataManager\Attributes\Persist;
use Technical\DataManager\Attributes\Link;
use Technical\DataManager\Attributes\Ignore;
use Technical\DataManager\Attributes\Event;
use ReflectionProperty;
use ReflectionClass;
use DateTime;

class SyncPersistDataService
{
    private static $specifiqueType = [
        DateTime::class
    ];
    public static function sync(ExternalDataSyncPort $externalDataSync)
    {
        $path = __DIR__.'/../..';
        $objectsMetadata = [];
        $allNamespaceSrc = FileTools::getAllNamespace($path);
        foreach($allNamespaceSrc as $namespace){
            $reflexionClass = new ReflectionClass($namespace);
            if(self::isInIgnoreNamespace($externalDataSync->getIgnoresNamespaces(),$reflexionClass->getNamespaceName())){
                continue;
            }
            if(!empty($reflexionClass->getAttributes(Persist::class)))
            {
              self::gennerateMetadataObjects($reflexionClass,$objectsMetadata);
            }
        }
        $externalDataSync::syncObjecsMetadata($objectsMetadata);
    }

    private static function gennerateMetadataObjects(ReflectionClass $reflexionClass,array &$objectsMetadata)
    {
        // format
        $format = 'data';
        if(!empty($reflexionClass->getAttributes(Persist::class))) {
            $attributesClass= $reflexionClass->getAttributes(Persist::class)[0]->getArguments();
            if(isset($attributesClass['format'])){
                $format = $attributesClass['format'];
            }
        }

        // trait
        $traitNames = [];
        foreach($reflexionClass->getTraits() as $trait ) {
            $traitNames[] = $trait->getName();
            self::gennerateMetadataObjects($trait,$objectsMetadata);
        }

        // interface 
        foreach($reflexionClass->getInterfaceNames() as $interfaceName) {
            if(!isset($objectsMetadata[$interfaceName])) {
                $objectsMetadata[$interfaceName] = [
                    'type' => 'interface',
                    'implementedClass' => [$reflexionClass->getName()],
                    'format' => $format
                ];
            } else {
                $objectsMetadata[$interfaceName]['implementedClass'][]=$reflexionClass->getName();
            }
        }

        // properties 
        $metadataProperties = [];
        foreach($reflexionClass->getProperties() as $reflexionProperty) {
            $reflexionProperty->setAccessible(true);
            if(!empty($reflexionProperty->getAttributes(Ignore::class))){
                continue;
            }
            $constraint = [];
            if($reflexionProperty->hasType()){
                $propertyType = $reflexionProperty->getType()->getName();
                if(class_exists($propertyType)){
                    $reflexionClassProperty = new ReflectionClass($propertyType);
                    if(empty($reflexionClassProperty->getAttributes(Persist::class))){
                        if(
                            !in_array($propertyType,self::$specifiqueType)
                            && !$reflexionClassProperty->isInterface()
                        ) {
                            continue;
                        }   
                    }
                }
                $constraint['nullable'] = $reflexionProperty->getType()->allowsNull();
            } else {
                $propertyType = null;
                $constraint['nullable'] = false;
            }

            if(!empty($reflexionProperty->getAttributes(Persist::class))){
                $attributesArguments = $reflexionProperty->getAttributes(Persist::class)[0]->getArguments();
                $constraint = array_merge($constraint,$attributesArguments);
            }
            if(!empty($reflexionProperty->getAttributes(Link::class))){
                $attributesArguments = $reflexionProperty->getAttributes(Link::class)[0]->getArguments();
                $constraint['link'] = $attributesArguments;
            }
            
            $metadataProperties[$reflexionProperty->getName()] = [
                'type' => $propertyType,
                'attributes' => [],
                'defaultValue' => $reflexionProperty->getDefaultValue(),
                'constraint' => $constraint
            ];
        }

        // methods
        $metadataMethods = [];
        foreach($reflexionClass->getMethods() as $reflexionMethod){
            if( $reflexionMethod->isPublic() && !empty($reflexionMethod->getAttributes(Event::class))) {
                $eventAttribute = $reflexionMethod->getAttributes(Event::class)[0];
                $metadataMethods[$reflexionMethod->getName()]=['event' => $eventAttribute->getArguments() ];
            }
        }
        // class
        if($reflexionClass->isTrait()){
            $ClassType = 'trait';
        } else {
            $ClassType = 'entity';
        }
        
        if($parentClass = $reflexionClass->getParentClass()){
            $parent = $parentClass->getName();
        }else {
            $parent = null;
        }
        $classGroup = explode('\\',$reflexionClass->getNamespaceName())[0];
        $objectsMetadata[$reflexionClass->getName()]=[
            'className' => $reflexionClass->getShortName(),
            'namespace' => $reflexionClass->getNamespaceName(),
            'fileName' => $reflexionClass->getFileName(),
            'type' => $ClassType,
            'classGroup' => $classGroup,
            'format'=>$format,
            'use' => [],
            'interface' => [],
            'trait'=> $traitNames,
            'parent' => $parent,
            'attributes' => [],
            'properties' => $metadataProperties,
            'methods' => $metadataMethods
        ];

        return $objectsMetadata;
    }
    
    private static function isInIgnoreNamespace(array $ignoresNamespaces, string $namespace): bool
    {
        foreach($ignoresNamespaces as $ignoreNamespace) {
            if(str_contains($namespace,$ignoreNamespace)){
                return true;
            }
        }
        return false;
    }
}
