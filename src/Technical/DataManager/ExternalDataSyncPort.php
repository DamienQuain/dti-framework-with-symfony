<?php

namespace Technical\DataManager;

interface ExternalDataSyncPort
{
    public static function syncObjecsMetadata(array $objectsMetadata);
    public static function getIgnoresNamespaces(): array;
}
