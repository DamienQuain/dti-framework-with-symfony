<?php
namespace Technical\DataManager;

use ReflectionClass;
use Domain\Common\Ports\DataManagerPort;
use DateTime;

class DataManagerService implements DataManagerPort
{
    public function __construct(
        private ExternalDataManagerPort $edm
    ) {
    }

    public function save(object $object):bool
    {
        $this->edm->save($object);
        return true;
    }

    public function load(string $classname,string $uuid): ?object
    {
        return $this->getRepository($classname)->findOneBy(['uuid' => $uuid]);
    }

    public function remove(Object $object):bool 
    {
        $this->edm->remove($object);
        return true;
    }

    public function getRepository(string $className): object
    {
        return new RepositoryManagerService($className,$this->edm);
    }

    public static function applyEvent(string $eventName, object $event)
    {
        ExternalDataManagerPort::applyEvent($eventName,$event);
    }
}
