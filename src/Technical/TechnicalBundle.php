<?php
namespace Technical;

use Technical\DependencyInjection\TechnicalExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class TechnicalBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new TechnicalExtension();
    }
}
