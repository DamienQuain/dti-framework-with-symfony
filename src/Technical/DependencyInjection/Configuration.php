<?php

namespace Technical\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('demo');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('test')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}