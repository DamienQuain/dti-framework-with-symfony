<?php

namespace Technical\File;

interface FileSystemInterface
{
    public function upload(string $filename,string $content);
    public function delete(string $fileName);
}
