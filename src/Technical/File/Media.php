<?php

namespace Technical\File;

use Technical\Serializer\Attributes as SE;
use Technical\DataManager\Attributes as DM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation as Serialize;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Domain\Media\MediaInterface;
use Domain\Common\Traits\TimestampableTrait;
use Domain\Common\Traits\IdTrait;

#[DM\Persist]
#[SE\Serialize]
class Media implements MediaInterface
{
    use IdTrait;
    use TimestampableTrait;
    
    #[Assert\NotNull(groups:[self::UPDATE])]
    #[DM\Ignore]
    private File $file;

    private ?string $directory;

    private string $filePath;

    private int $fileSize;

    private string $fileExtension;

    private string $fileMimeType;

    #[Serialize\Groups([self::MINIMAL])]
    public function getContentUrl()
    {
        return FileManagerService::BaseContentUrlRoute.'/'.$this->getFilePath();
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile( File $file)
    {
        $this->file = $file;
        if ($file instanceof UploadedFile) {
            $this->setFileExtension($file->getClientOriginalExtension());
            $this->setFileMimeType($file->getMimeType());
            $this->setFileSize($file->getSize());
        }
        return $this;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath)
    {
        $this->filePath = $filePath;
        return $this;
    }

    public function getDirectory()
    {
        return $this->directory;
    }

    public function setDirectory(?string $directory)
    {
        $this->directory = $directory;
        return $this;
    }

    public function getFileSize()
    {
        return $this->fileSize;
    }

    public function setFileSize(?string $fileSize)
    {
        $this->fileSize = $fileSize;
        return $this;
    }

    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    public function setFileExtension(?string $fileExtension)
    {
        $this->fileExtension = $fileExtension;
        return $this;
    }

    public function getFileMimeType()
    {
        return $this->fileMimeType;
    }

    public function setFileMimeType(?string $fileMimeType)
    {
        $this->fileMimeType = $fileMimeType;
        return $this;
    }
}
