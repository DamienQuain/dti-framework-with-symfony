<?php
namespace Technical\File;

use Technical\DataManager\DataManagerService;
use Domain\Common\Ports\CUDPort;

class MediaService implements CUDPort
{
    public function __construct(
        private DataManagerService $dm,
        private FileManagerService $fm
    )
    {
    }

    public function create(object &$object, array $context = [])
    {}

    public function update(object $object, array $context = [])
    {}

    public function delete(object $object, array $context = [])
    {
        $this->dm->remove($object);
        $this->fm->delete($object);
    }
}
