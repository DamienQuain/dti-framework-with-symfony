<?php

namespace Technical\File\Generator;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\ClassType;

class FilePhpGeneratorService
{
    public static function generatesFiles(array $objectsMetadata)
    {
        foreach($objectsMetadata as $entityName => $objectMetadata){
            if($objectMetadata['type'] == 'interface') {
                continue;
            }
            
            $content = new PhpFile;
            $content->addComment('##This part is auto-generated##');
            $namespace = $content->addNamespace($objectMetadata['namespace']);
            $class =  match($objectMetadata['type']){
                'entity' => $namespace->addClass($objectMetadata['className']),
                'trait' => $namespace->addTrait($objectMetadata['className'])
            };
            
            // use trait
            $ignoreProperties = [];
            foreach($objectMetadata['trait'] as $traitName ) {
                if(isset($objectsMetadata[$traitName])) {
                    $namespace->addUse($objectsMetadata[$traitName]['namespace'].'\\'.$objectsMetadata[$traitName]['className']);
                    $class->addTrait($objectsMetadata[$traitName]['namespace'].'\\'.$objectsMetadata[$traitName]['className']);
                    $ignoreProperties = [
                        ...$ignoreProperties,
                        ...array_keys($objectsMetadata[$traitName]['properties'])
                    ];
                } else {
                    $namespace->addUse($traitName);
                    $class->addTrait($traitName);
                }
            }
            // add other use
            foreach($objectMetadata['use'] as $use) {
                if(isset($use['alias'])) {
                    $namespace->addUse($use['name'],$use['alias']);
                } else {
                    $namespace->addUse($use['name']);
                }
                    
            }

            foreach($objectMetadata['interface'] as $interface) {
                $namespace->addUse($interface);
                $class->addImplement($interface);
            }

            if(!is_null($objectMetadata['parent'])) {
                $parent = $objectMetadata['parent'];
                if(isset($objectsMetadata[$parent])) {
                    $parent = $objectsMetadata[$parent];
                    $ignoreProperties = [
                        ...$ignoreProperties,
                        ...array_keys($parent['properties'])
                    ];
                    $parent = $parent['namespace'].'\\'.$parent['className'];
                }
                if(!str_contains($parent,$objectMetadata['namespace'])){
                    $namespace->addUse($parent);
                }
                $class->addExtend($parent);
                
            }
            // add class attribute
            foreach($objectMetadata['attributes'] as $attribute) {
                if(isset($attribute['arguments'])) {
                    $class->addAttribute($attribute['name'],$attribute['arguments']);
                } else {
                    $class->addAttribute($attribute['name']);
                }
            }

            self::addProperties($class,$objectMetadata['properties'],$ignoreProperties);
            self::addMethods($class,$objectMetadata['methods']);
            self::persistPhpFile($content,$objectMetadata['fileName']);

        }
    }

    private static function addProperties(ClassType $class,array $metadataProperties,array $ignoreProperties)
    {
        foreach($metadataProperties as $propertyName => $property) {
            if(!$class->hasProperty($propertyName) && !in_array($propertyName,$ignoreProperties)){
                $classProperty = $class->addProperty($propertyName);
                $classProperty->setType($property['type']);
                if(isset($property['constraint']['nullable'])) { 
                    $classProperty->setNullable($property['constraint']['nullable']);
                }
                if(!is_null($property['defaultValue'])){
                    $classProperty->setValue($property['defaultValue']);
                }
                // add property attribute
                foreach($property['attributes'] as $attribute) {
                    if(isset($attribute['arguments'])) {
                        $classProperty->addAttribute($attribute['name'],$attribute['arguments']);
                    } else {
                        $classProperty->addAttribute($attribute['name']);
                    }
                }
            }
        }
    }

    private static function addMethods(ClassType $class,array $metadataMethods)
    {
        foreach ($metadataMethods as $metadataMethod) {
            $method = $class->addMethod($metadataMethod['name']);
            $method->setStatic($metadataMethod['static']);
            $method->setReturnType($metadataMethod['returnType']);
            foreach($metadataMethod['parameters'] as $metadataMethodParameter) {
                $method->addParameter($metadataMethodParameter['name']);
            }
            foreach($metadataMethod['attributes'] as $metadataMethodAttribute) {
                $method->addAttribute($metadataMethodAttribute['name'],$metadataMethodAttribute['arguments']);
            }
            $method->addBody($metadataMethod['body']);
        }
    }
    
    private static function persistPhpFile(PhpFile $content,$fileName)
    {
        $content=$content->__toString();
        $content = preg_replace('/}$/', "    #### end of autogenerated ####",$content);
        if(
            file_exists($fileName)
        ){
            $contentFile = file_get_contents($fileName);
            $content = preg_replace('/^(\S|\s)*#### end of autogenerated ####\n/', $content,$contentFile);
        } else {
            $content.="\n}";
            if(!is_dir(dirname($fileName))){
                mkdir(directory: dirname($fileName),recursive: true);
            }
        }
        file_put_contents($fileName,$content);
    }
}
