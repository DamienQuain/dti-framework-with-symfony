<?php

namespace Technical\File\Generator;

use XMLWriter;

class FileXmlGeneratorService
{
    public static function generatesFiles(array $xmlsMetadata)
    {
        $xw = new XMLWriter();
        foreach($xmlsMetadata as $xmlMetadata) {
            $xw->openMemory();
            $xw->startDocument('1.0','utf-8');
            $xw->setIndent(TRUE);  
            self::createElements($xw,$xmlMetadata['content']);
            $xw->endDocument();
            self::persistXmlFile($xw,$xmlMetadata['fileName']);
            $xw->flush();
        }   
    }

    private static function createElements(XMLWriter $xw, array $xmlElementsMetadata)
    {
        foreach($xmlElementsMetadata as $xmlElementName => $xmlElementValue ){
            $xmlElementName = explode('_',$xmlElementName);
            $xw->startElement($xmlElementName[0]);
            self::createAttributes($xw,$xmlElementValue);
            $xw->endElement();
        }
    }

    private static function createAttributes(XMLWriter $xw, array $xmlAttributesMetadata)
    {
        foreach($xmlAttributesMetadata as $xmlAttributeName => $xmlAttributeValue ){
            if(!is_array($xmlAttributeValue)){
                $xw->writeAttribute($xmlAttributeName,$xmlAttributeValue);
                unset($xmlAttributesMetadata[$xmlAttributeName]);
            }
        }
        foreach($xmlAttributesMetadata as $xmlAttributeName => $xmlAttributeValue ){
            self::createElements($xw,[
                $xmlAttributeName => $xmlAttributeValue
            ]);
        }
    }

    private static function persistXmlFile($xw,$fileName)
    {
        $content=$xw->outputMemory();
        file_put_contents($fileName,$content);
    }
}
