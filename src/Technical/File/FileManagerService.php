<?php

namespace Technical\File;

class FileManagerService
{
    const BaseContentUrlRoute = '/medias';
    public function __construct(private FileSystemInterface $fs)
    {
    }

    public function upload(Media $media) 
    {
        $media->setFilePath(
            $media->getUuid().'.'.$media->getFileExtension()
        );
        $this->fs->upload($media->getFilePath(),$media->getFile()->getContent());
    }

    public function delete(Media $media)
    {
        $this->fs->delete($media->getFilePath());
    }
}