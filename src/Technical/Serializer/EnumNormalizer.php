<?php
namespace Technical\Serializer;

use UnitEnum;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use BackedEnum;

class EnumNormalizer implements ContextAwareDenormalizerInterface, ContextAwareNormalizerInterface
{
    public function denormalize($data, string $type, ?string $format = null, array $context = []):mixed
    {
        $enum = new $type();
        if($enum instanceof BackedEnum) {
            return $enum[0]::from($enum[1]);
        } else {
            $static = 'return '.$enum[0].'::'.$enum[1].';';
            return eval($static);
        }
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []):bool
    {
        return enum_exists($type);
    }

    public function normalize(mixed $object, string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        if(isset($object->value)) {
            return $object->value;
        } else {
            return $object->name;
        }
    }

    public function supportsNormalization($data, ?string $format = null, array $context = []):bool
    {
        return ($data instanceof UnitEnum);
    }
}