<?php

namespace Technical\Serializer;

use Technical\Serializer\Attributes\Serialize;
use Technical\DataManager\DataManagerService;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use ReflectionClass;
use Exception;
use Doctrine\Common\Proxy\Proxy;

class TechnicalObjectNormalizer implements ContextAwareDenormalizerInterface, ContextAwareNormalizerInterface
{
    public function __construct(
        private ObjectNormalizer $normalizer,
        private DataManagerService $dm)
    {
    }

    /**
     * function for denormalize data with load bdd entity
     *
     * @param [type] $data
     * @param string $type
     * @param string|null $format
     * @param array $context
     * @return void
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []):mixed
    {
        if (is_string($data) && preg_match('/.*\-.*\-.*\-.*\-.*/',$data)) {
            $data = array('uuid' => $data);
        }
        if(isset($data['uuid'])) {
            
            $object = $this->dm->load($type,$data['uuid']);
            if(is_null($object)) {
                throw new Exception("the uuid for $type does not exist");
            }
            if(is_array($data)){
                $context = array_merge([
                    AbstractNormalizer::OBJECT_TO_POPULATE => $object,
                ],$context);
            }
        }
        $context = array_merge([
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true
        ],$context); 
        return $this->normalizer->denormalize($data,$type,$format,$context);
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []):bool
    {
        if(class_exists($type)){
            $reflexionClass = new ReflectionClass($type);
            if(!empty($reflexionClass->getAttributes(Serialize::class)))
            {
                return true;
            }
        }
        return false;
    }

    public function normalize(mixed $object, string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $context = array_merge([
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getUuid();
            },
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['__isInitialized__','__cloner__','__initializer__'],
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true
        ],$context);
        return $this->normalizer->normalize($object,$format,$context);
    }

    public function supportsNormalization($data, ?string $format = null, array $context = []):bool
    {
        if (is_object($data)) {
            $class = ($data instanceof Proxy)
                ? get_parent_class($data)
                : get_class($data);
            $reflexionClass = new ReflectionClass($class);
            if(!empty($reflexionClass->getAttributes(Serialize::class)))
            {
                return true;
            }
        }
        return false;
    }
}
