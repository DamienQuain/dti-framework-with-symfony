<?php
namespace Technical\Serializer;

interface TraitsSerializerInterface
{
    const MINIMAL = 'trait:minimal';
    const CREATE_ONLY = 'trait:create';
    const READ = 'trait:read';
    const UPDATE = 'trait:update';
}