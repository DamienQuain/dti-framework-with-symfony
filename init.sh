#!/bin/bash

composer install

# clean inutile folder 
if [ -d "migrations" ] 
then
    rm -rf migrations;
fi
if [ -d "src/Controller" ] 
then
    rm -rf src/Controller ;
fi
if [ -d "src/Entity" ] 
then
    rm -rf src/Entity;
fi
if [ -d "src/Repository" ] 
then
    rm -rf src/Repository;
fi
if [ -f "src/Kernel.php" ] 
then
    rm -f src/Kernel.php;
fi

./bin/console doctrine:migration:migrate