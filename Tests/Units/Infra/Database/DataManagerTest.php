<?php
namespace Tests\Units\Infra\Database;

use Tests\Units\Technical\DataManager\Stubs\LazyDataLoadManagerStub;
use Tests\Units\Infra\Database\Stubs\EntityManagerStub;
use Technical\Security\TechnicalUser;
use PHPUnit\Framework\TestCase;
use Infra\Database\Adapters\DoctrineManagerAdapter;

class DataManagerTest extends TestCase
{
    public function testgetExternalClassNameWithObjectClassName() 
    {
        $dataManager =  new DoctrineManagerAdapter(
            new EntityManagerStub(),
            new LazyDataLoadManagerStub()
        );

        $externalClassName = $dataManager->getExternalClassNameWithObjectClassName(TechnicalUser::class);
        $this->assertEquals('Infra\Database\Entity\Technical\Security\TechnicalUser',$externalClassName);
    }

    
}