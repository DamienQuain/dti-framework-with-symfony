<?php

namespace Tests\Units\Infra\Database\Stubs;

use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Proxy\ProxyFactory;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManagerInterface;

class EntityManagerStub implements EntityManagerInterface
{

    public function getRepository($className) 
    {

    }

    public function getCache() 
    {

    }

    public function getConnection()
    {
        return null;
    }

    public function getExpressionBuilder()
    {

    }

    public function beginTransaction()
    {

    }

    public function transactional($func)
    {

    }

    public function commit()
    {

    }

    public function rollback()
    {

    }

    public function createQuery($dql = '')
    {

    }

    public function createNamedQuery($name)
    {

    }

    public function createNativeQuery($sql, ResultSetMapping $rsm)
    {
        return new NativeQuery($this);
    }

    public function createNamedNativeQuery($name)
    {

    }

    public function createQueryBuilder()
    {

    }

    public function getReference($entityName, $id)
    {

    }

    public function getPartialReference($entityName, $identifier)
    {

    }

    public function close()
    {

    }

    public function copy($entity, $deep = false)
    {

    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {

    }

    public function getEventManager()
    {

    }

    public function getConfiguration()
    {

    }

    public function isOpen()
    {

    }

    public function getUnitOfWork()
    {

    }

    public function getHydrator($hydrationMode)
    {

    }

    public function newHydrator($hydrationMode)
    {

    }

    public function getProxyFactory()
    {
        return null;
    }

    public function getFilters()
    {

    }

    public function isFiltersStateClean()
    {

    }

    public function hasFilters()
    {

    }

    public function getClassMetadata($className)
    {
        return new ClassMetadata($className);
    }

    public function find($className, $id)
    {

    }

    public function persist($object)
    {

    }

    public function remove($object)
    {

    }

    public function merge($object)
    {
        
    }

    public function clear($objectName = null)
    {

    }

    public function detach($object)
    {

    }

    public function refresh($object)
    {

    }

    public function flush()
    {

    }

    public function getMetadataFactory()
    {

    }

    public function initializeObject($obj)
    {
        
    }

    public function contains($object)
    {

    }

    public function wrapInTransaction(callable $func)
    {

    }
}