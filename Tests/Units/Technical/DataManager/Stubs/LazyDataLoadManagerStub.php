<?php
namespace Tests\Units\Technical\DataManager\Stubs;

use Technical\DataManager\LazyDataLoad\LazyDataLoadManagerInterface;
use Technical\DataManager\ExternalDataManagerPort;

class LazyDataLoadManagerStub implements LazyDataLoadManagerInterface
{
    public function getLazyLoadProxy(object $entity,string $objectClassName,ExternalDataManagerPort $edm)
    {
        return null;
    }
}